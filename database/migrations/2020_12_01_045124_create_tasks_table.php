<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('project')->nullable();
            $table->bigInteger('user')->nullable();
            $table->bigInteger('assigned_to')->nullable();
            $table->string('number')->nullable();
            $table->text('participants')->nullable();
            $table->text('observers')->nullable();
            $table->text('name')->nullable();
            $table->longText('description')->nullable();
            $table->string('completed_on')->nullable();
            $table->string('deadline')->nullable();
            $table->enum('priority', ['0', '1', '2'])->default('0')->nullable();
            $table->enum('status', ['0', '1', '2', '3'])->default('0')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
