import axios from "axios";
import router from '../../router'
export default {
  namespaced: true,

  state: {
     access_detail:null
  },

  getters: {    
  },

 mutations: {
   SET_ACCESS_DETAIL(state, accessDetail) {
      state.access_detail = accessDetail;
    },
  },


  actions: {
     async saveAccessDetail({ commit }, data) {
     await axios.post(  '/api/projects/' + data.id + '/access_detail',data)
       .then(function (response) {         
          commit("SET_ACCESS_DETAIL",response.data.access_detail);  
     
      })
        }
  },
}
