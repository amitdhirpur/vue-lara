import axios from "axios";
import router from '../../router'
export default {
  	namespaced: true,
  	state: {
     	userData: null,
     	isAuthenticated:localStorage.getItem("api_token"),
     	user:{},
      loginMessage:''
  	},
  	getters: {
	    user: state => state.userData,
      authUser: state => state.user,
	    loginMessage: state => state.loginMessage
  	},
 	mutations: {
	    setAuthToken(state, token) {
	      	state.isAuthenticated = token ?? null;
	    },
	    setUser(state, data) {
	      	if(typeof data == 'undefined' || data === null) {
	        	state.user={};
	      	} else {
		        state.user.profile =data.profile;
		        state.user.name =data.name;
		        state.user.id =data.id;
	      	} 
	    },
        setloginMessage(state, data) {
          state.loginMessage = data;
      }
  	},
  	actions: {
     	async sendLoginRequest({ commit }, data) {
        	commit("setErrors", {}, { root: true });
         	await axios.post(  '/api/login', data)
          	.then(function (response) {
              if(response.data.error){
                commit("setloginMessage", response.data.error);
                setTimeout(function(){
                   commit("setloginMessage","");
                },3000);
              }else{
                
                localStorage.setItem("api_token", response.data.access_token);
                localStorage.setItem("user", response.data.user);
                localStorage.setItem("type", response.data.type);
                localStorage.setItem("date_format", response.data.date_format);
                commit("setAuthToken",localStorage.getItem("api_token"));  
                window.location.replace('/dashboard');          
               // router.push({ path: 'dashboard' });

              }

          	})
        },
       	async sendSocialLoginRequest({ commit }, data) {
          	await axios.post('/api/sociallogin/'+data.provider, {})
          	.then(function (response) {
              	if(response.data.url) {
                 	window.location.href=response.data.url;
              	}
          	});
        },
        async socialLoginRequest({ commit }, data) {
           	await axios.post( data.path, {})
            .then(function (response) {
              	localStorage.setItem("api_token", response.data.access_token);
              	localStorage.setItem("user", response.data.user);
              	localStorage.setItem("type", response.data.type);
              	localStorage.setItem("date_format", response.data.date_format);
              	commit("setAuthToken",localStorage.getItem("api_token"));      
              	window.location.replace('/dashboard'); 
            });
        },
        async sendLogoutRequest({ commit }, data) {
           	await axios.post(data.path,{}).then(function (response) {
              	localStorage.clear();
              	commit("setAuthToken",localStorage.getItem("api_token"));
              	router.push({ name: 'Auth' });
            });
        },
        async getAuthUser({ commit }, data) {
           	if(localStorage.getItem("api_token")) {
	            await axios.get(data.path).then(function (response) { 
	                commit("setUser",{
	                  	profile:response.data.user.profile,
	                  	name:response.data.user.name,
	                  	id:response.data.id
	                });
	            })
          	}
        }
	}
}
