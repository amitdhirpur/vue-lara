<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api'], function ($router) {
    Route::post('add_pic', 'UsersController@add_pic');
    Route::post('save_token', 'UsersController@save_token');
    Route::get('menu', 'MenuController@index');
    Route::post('salesportal', 'PortalApiController@handleSalesPortalRequest');
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('register', 'AuthController@register');

    Route::post('sociallogin/{provider}', 'SocialAuthController@socialSignup');
    Route::post('sociallogin/{provider}/callback', 'SocialAuthController@handleGoogleCallback');

    Route::resource('notes', 'NotesController');

    Route::get('/site-logo', 'TasksController@getLogo');
    Route::get('/timezone', 'TasksController@timezone');

    Route::resource('resource/{table}/resource', 'ResourceController');

    Route::group(['middleware' => 'admin'], function ($router) {

        Route::resource('mail',        'MailController');
        Route::get('prepareSend/{id}', 'MailController@prepareSend')->name('prepareSend');
        Route::post('mailSend/{id}',   'MailController@send')->name('mailSend');
        Route::resource('bread',  'BreadController');   //create BREAD (resource)
        Route::prefix('menu/menu')->group(function () {
            Route::get('/',         'MenuEditController@index')->name('menu.menu.index');
            Route::get('/create',   'MenuEditController@create')->name('menu.menu.create');
            Route::post('/store',   'MenuEditController@store')->name('menu.menu.store');
            Route::get('/edit',     'MenuEditController@edit')->name('menu.menu.edit');
            Route::post('/update',  'MenuEditController@update')->name('menu.menu.update');
            Route::get('/delete',   'MenuEditController@delete')->name('menu.menu.delete');
        });
        Route::prefix('menu/element')->group(function () {
            Route::get('/',             'MenuElementController@index')->name('menu.index');
            Route::get('/move-up',      'MenuElementController@moveUp')->name('menu.up');
            Route::get('/move-down',    'MenuElementController@moveDown')->name('menu.down');
            Route::get('/create',       'MenuElementController@create')->name('menu.create');
            Route::post('/store',       'MenuElementController@store')->name('menu.store');
            Route::get('/get-parents',  'MenuElementController@getParents');
            Route::get('/edit',         'MenuElementController@edit')->name('menu.edit');
            Route::post('/update',      'MenuElementController@update')->name('menu.update');
            Route::get('/show',         'MenuElementController@show')->name('menu.show');
            Route::get('/delete',       'MenuElementController@delete')->name('menu.delete');
        });
        Route::get('/roles/move/move-up',      'RolesController@moveUp')->name('roles.up');
        Route::get('/roles/move/move-down',    'RolesController@moveDown')->name('roles.down');
        // settings routes
        Route::prefix('settings')->group(function ($router) {
            Route::get('/', 'SettingsController@index');
            Route::post('/store', 'SettingsController@store');
        });
         // Daily Report api url
             Route::get('daily-report', 'DailyReportController@index');
             
             Route::get('daily-report/{id}', 'DailyReportController@dailyReport');
             Route::post('daily-report/filter-reports', 'DailyReportController@filterReport');

    });

    Route::group(['middleware' => 'auth:api'], function ($router) {
        Route::resource('roles',        'RolesController');
        Route::resource('designations',        'PositionsController');
        Route::post('/user/tag', 'UsersController@tagUsers');
        Route::resource('users', 'UsersController');
         Route::post('users/zip-files', 'UsersController@downloadZipFolder');
        Route::post('users/{id}/save', 'UsersController@save');   
        Route::prefix('user')->group(function ($router) {     
            Route::get('', 'UsersController@user');
            Route::post('profile/{id}/update', 'UsersController@updateUserProfile');
            Route::post('password/{id}/update', 'UsersController@updatePassword');
        });
         // archive-users api url.
        Route::prefix('archive-users')->group(function ($router) {
            Route::get('', 'UsersController@archiveUsers');
            Route::post('restore', 'UsersController@archiveRestore');
        });
        // Tasks routes
        Route::prefix('tasks')->group(function ($router) {
            Route::get('/', 'TasksController@index');
            Route::post('/add', 'TasksController@save');
            Route::get('/edit/{id}', 'TasksController@edit');
            Route::post('/update', 'TasksController@update');
            Route::post('/bulk-action', 'TasksController@bulkUpdate');
            Route::get('/delete/{id}', 'TasksController@delete');
            Route::get('/view/{id}', 'TasksController@view');
            Route::post('/filter-tasks', 'TasksController@filterTasks');
            Route::get('/project-name/{id}', 'TasksController@projectName');
        });

        //Comments Routes
        Route::prefix('comment')->group(function ($router) {
            Route::post('/add', 'CommentController@add');
            Route::post('/list', 'CommentController@list');
            Route::post('/update', 'CommentController@update');
            Route::post('/more', 'CommentController@countMore');
            Route::post('/feeds-more', 'CommentController@feedsCountMore');
            Route::post('/delete', 'CommentController@delete');
        });
        // projects api url.
        Route::prefix('projects')->group(function ($router) {
            Route::get('/list', 'ProjectController@listProjects');
            Route::post('/{id}/access_detail', 'ProjectController@saveAccessDetail');
        });
         // archive-projects api url.
        Route::prefix('archive-projects')->group(function ($router) {
            Route::get('', 'ProjectController@archiveProjects');
            Route::post('restore', 'ProjectController@archiveRestore');
        });
        Route::resource('projects', 'ProjectController');
        // logs api url.
        Route::prefix('logs')->group(function ($router) {
            Route::get('/', 'LogsController@index');
        });
        // drives api url.
        Route::prefix('drives')->group(function ($router) {
            Route::get('/', 'DrivesController@index');
            Route::post('/add', 'DrivesController@add');
            Route::post('/search-drive', 'DrivesController@searchDrive');
            Route::prefix('folder')->group(function ($router) {
                Route::post('/delete', 'DrivesController@delete');
                Route::post('/rename', 'DrivesController@renameFolderOrFile');
                Route::post('/add', 'DrivesController@addFolder');
            });
            Route::get('/{id}/files', 'DrivesController@deriveFiles');
            Route::get('{all}', 'DrivesController@getFolderAndFiles')->where('all', '.*');
            Route::post('/zip-files', 'DrivesController@downloadZipFolder');

        });

        // chat api routes
        Route::prefix('chat')->group(function ($router) {
            Route::get('/', 'ChatController@index');
            Route::post('/message', 'ChatController@save');
            Route::post('/edit', 'ChatController@update');
            Route::get('/delete/{id}/{user}', 'ChatController@delete');
            Route::get('/users', 'ChatController@getUser');
            Route::get('/get-chat', 'ChatController@realtime');
            Route::get('/setChatNotiStatus', 'ChatController@setNotiStatus');
        });

        // feeds api url.
        Route::prefix('feeds')->group(function ($router) {
            Route::get('/', 'FeedController@index');
            Route::post('/add', 'FeedController@add');
            Route::post('/edit/{id}', 'FeedController@update');
            Route::post('/search', 'FeedController@search');
            Route::post('/delete', 'FeedController@delete');
        });
        // like api url
        Route::post('/like', 'LikesController@like');
        Route::post('/getlikelist', 'LikesController@getlikelist');
         // feeds api url.
        Route::prefix('notifications')->group(function ($router) {
             Route::get('/', 'NotificationController@index');
             Route::get('/update-uncheck', 'NotificationController@updateUncheck');
             Route::get('/feedUncheck', 'NotificationController@feedUncheck');
        });
        // Daily Report api url
        Route::prefix('report')->group(function ($router) {  
             Route::post('/save', 'DailyReportController@save');
             Route::get('', 'DailyReportController@report');
        });

         // Leave Approval
        Route::prefix('leaves')->group(function ($router) {  
            Route::post('/send', 'LeaveController@save');
            Route::get('/', 'LeaveController@index');
            Route::get('/view/{id}', 'LeaveController@show');
            Route::post('/edit', 'LeaveController@update');
            Route::post('/responde', 'LeaveController@responde');
            Route::get('/user-list', 'LeaveController@getUsers');
        });
        Route::get('report-users', 'DailyReportController@listUsers');
        // **************  Extra url's *******************
        Route::post('/notify-token', 'TasksController@save_token');
        Route::post('/search-globally', 'UsersController@globalSearch');




        Route::prefix('upload')->group(function ($router) {
            Route::post('/file', 'UploadController@index');
        });

    });
});

Route::get('/test', 'TasksController@testing');