
<!DOCTYPE html>
<html>
  <head>
    <title>Please confirm your e-mail</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
  </head>
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">
    <table bgcolor="#F4F4F4" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
            <tr style="display: none">
                <td align="center">
                    <table border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;" width="100%">
                        <tbody>
                            <tr>
                                <td align="center" style="padding: 20px 10px 40px 10px;" valign="top"></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" style="padding: 0px 10px 0px 10px;">
                    <table border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;background: #001d4a;" width="100%">
                        <tbody>
                            <tr>
                                <td align="center" style="padding: 10px 30px 10px 30px;border-radius: 4px 4px 0px 0px;color: #111111;font-family: 'Open Sans', sans-serif;font-size: 48px;font-weight: 400;letter-spacing: 3px;line-height: 48px;" valign="center">
                                    <h1 style="font-size: 34px;font-weight: 400;text-align: left;margin: 0;color: #fff;word-break: break-all;">{{$subject}}</h1>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" style="padding: 0px 10px 0px 10px;">
                    <table border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;" width="100%">
                        <tbody>
                            <tr style="display: none">
                                <td align="left" bgcolor="#FFFFFF" style="padding: 20px 30px 20px 30px;color: #666666;font-family: 'Open Sans', sans-serif;font-size: 15px;font-weight: 400;line-height: 25px;">
                                    <p style="margin: 0;padding-bottom: 5px;">Hello <span style=" font-weight: 600;">,</span></p>
                                    <p style="margin: 0;padding-bottom: 5px;">Thanks for contacting us..</p>
                                    <p style="margin: 0;padding-bottom: 5px;">Here is your inserted data with form:</p>
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="#FFFFFF" class="fluid-img img-center pb70" style="font-size:0pt;line-height:0pt;text-align:center;padding: 0px 30px 0px 30px;"><img alt="" border="0" height="1" src="images/separator.jpg" style=" opacity: 0.1;" width="100%">
                                </td>
                            </tr>
                            <tr>
                                <td align="left" bgcolor="#FFFFFF">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                                <td align="center" style="padding: 20px 30px 40px 30px;">
                                                    <table border="0" cellpadding="1" cellspacing="0" style="width: 100%;color: #353535;font-family: 'Open Sans', sans-serif;font-size: 14px;border-color: #febc1c;">
                                                        <tbody>
                                                           
                                                            <tr>
                                                                <td style="padding: 4px 0px;"><span style="font-size: 15px;color: #febc1c;font-weight: 600;padding-right: 5px;border-left: 5px #ececec;padding-left: 5px;border-left-style: double;width: 130px;display: inline-block;">Email</span>
                                                                    <span>
                                                                    <span style=" padding-right: 2px;">:</span>{{$from_email}}
                                                                   </span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 4px 0px;">
                                                                    <span style="font-size: 14px;color: #febc1c;font-weight: 600;padding-right: 5px;border-left: 5px #ececec;padding-left: 5px;border-left-style: double;width: 130px;display: inline-block;">Feedback</span> 
                                                                    <span><span style=" padding-right: 2px;">:</span>Your leave application is {{$status}}.</span>
                                                                </td>
                                                            </tr>
                                                            @if($reason)
                                                            <tr>
                                                                <td style="padding: 4px 0px;">
                                                                    <span style="font-size: 14px;color: #febc1c;font-weight: 600;padding-right: 5px;border-left: 5px #ececec;padding-left: 5px;border-left-style: double;width: 130px;display: inline-block;">Reason</span> 
                                                                    <span><span style=" padding-right: 2px;">:</span>{{$reason}}</span>
                                                                </td>
                                                            </tr>
                                                            @endif
                                                            <tr>
                                                                <td style="padding: 4px 0px;">
                                                                    <span style="font-size: 14px;color: #febc1c;font-weight: 600;padding-right: 5px;border-left: 5px #ececec;padding-left: 5px;border-left-style: double;width: 130px;display: inline-block;">
                                                                        <a href="{{$url}}"><button style="color: #fff;background-color: #001d4a;border-color: #001d4a;transition: 0.3s all ease-in-out;display: inline-block;font-weight: 400;text-align: center;vertical-align: middle;cursor: pointer;user-select: none;border: 1px solid transparent;padding: 0.375rem 0.75rem;font-size: 0.875rem;line-height: 1.5;border-radius: 0.25rem;cursor: pointer;">Click here</button></a></span> 
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" style="padding: 18px 10px 0px 10px;">
                    <table border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;border-radius: 4px 4px 4px 4px;background: #001d4a;" width="100%">
                        <tbody>
                            <tr>
                                <td align="left" style="padding: 12px 10px 12px 30px;font-family: 'Open Sans', sans-serif;font-size: 14px;line-height: 0;font-weight: 400;color: #ffffff;margin: 0;text-align: left;width: 10%;" valign="center">
                                    <img src="{{url('assets/images/teqtop-white.png')}}" style="border: 0px;" width="95">
                                </td>
                                <td align="left" style="padding: 12px 30px 12px 10px;font-family: 'Open Sans', sans-serif;font-size: 14px;line-height: 25px;font-weight: 400;color: #ffffff;margin: 0;text-align: left;" valign="center">
                                    <p style="margin: 0;line-height: 19px;display: block;">
                                        <span style="display: block;">Office No. 801, 8th Floor, Tower -A, Bestech Business Tower, Sector 66, Mohali 160066 ( India )</span>
                                        </span>
                                    </p>
                                </td>
                                <td align="right" style="padding: 12px 30px 12px 30px;color: #ffffff;font-family: 'Open Sans', sans-serif;font-size: 14px;font-weight: 400;line-height: 0;" valign="center">
                                    <span><a href="#" style="display: inline-block;padding: 4px 9px;line-height: 0;"><img alt="" src="images/instagram.png" width="20px"></a>
                                    </span> 
                                    <span><a href="#" style="display: inline-block;padding: 4px 2px;line-height: 0;"><img alt="" src="images/twitter.png" width="20px"></a>
                                    </span> 
                                    <span><a href="#" style="display: inline-block;padding: 4px 4px;line-height: 0;"><img alt="" src="images/facebook.png" width="20px"></a>
                                    </span> 
                                    <span><a href="#" style="display: inline-block;padding: 4px;line-height: 0;"><img alt="" src="images/linkedin.png" width="20px"></a>
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;" width="100%">
                        <tbody>
                            <tr>
                                <td align="center" style="padding: 20px 10px 40px 10px;" valign="top"></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
