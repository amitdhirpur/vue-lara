<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\User;
use Mail;
use File;
use Artisan;

class SendNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $tokens;
    protected $title;
    protected $body;
    protected $thumb;
    protected $url;

    public function __construct($tokens,$title,$body,$thumb,$url)
    {
        $this->tokens = $tokens;
        $this->title = $title;
        $this->body = $body;
        $this->thumb = $thumb;
        $this->url = $url;
        // create & initialize a curl session
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $this->send_noti();
       // File::put('/var/www/projects/projectportal/projectportal/laravel/mytextdocument11111.txt','John Doe');
        User::send_notification($this->tokens, $this->title ,$this->body, $icon = null, $action = null);

    }

    public function send_noti(){
        // exec('php artisan queue:work --once');
        $curl = curl_init();

        // set our url with curl_setopt()
        curl_setopt($curl, CURLOPT_URL, url("/sendnotification"));

        // return the transfer as a string, also with setopt()
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        // curl_exec() executes the started curl session
        // $output contains the output string
        $output = curl_exec($curl);

        // close curl resource to free up system resources
        // (deletes the variable made by curl_init)
        curl_close($curl);
    }
}
