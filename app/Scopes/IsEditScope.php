<?php
  
namespace App\Scopes;
  
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Models;
use Illuminate\Database\Eloquent\Scope;
use User;
class IsEditScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Models $model)
    {
         return ($builder->menuroles == 'admin' || $builder->menuroles == 'HR' || $builder->menuroles == 'Team Lead' || $builder->menuroles == 'manager') ? true : false;
    }
}