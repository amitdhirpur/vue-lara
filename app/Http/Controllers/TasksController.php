<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\Models\Project;
use App\Models\Comment;
use App\Models\Setting;
use App\User;
use Auth, DB, DataTables;
use App\DriveFolder;
use File, Storage;
use App\Models\Notification;
use App\Models\UserNotification;

class TasksController extends Controller
{
    /**
     * List all the tasks according to the user authenticated.
     * @param  \Illuminate\Http\Request
    */
    protected function listTasks($request) {
        $__id = Auth::user()->id;

        $tasks = Task::leftJoin('users as from', 'tasks.user', '=', 'from.id')
            ->leftJoin('users as to', 'tasks.assigned_to', '=', 'to.id')
            ->leftJoin('projects', 'tasks.project', '=', 'projects.id')
            ->leftJoin('folders', 'folders.project_id', '=', 'tasks.project')
            ->select('tasks.*', 'from.name as created_by', 'to.name as responsible_person', 'projects.name as project_name','projects.id as project_id','folders.id as folder_id','folders.name as folder_name');

        if(empty($request)) {
            $tasks = $tasks->where('tasks.status', '!=', '1');
        }

        if(!empty($request)) {
            if(array_key_exists('status', $request)) {
                if($request['status'] || $request['status'] == '0') {
                    $tasks = $tasks->where('tasks.status', $request['status']);
                } else {
                    $tasks = $tasks->where('tasks.status', '!=', '1');
                }
            } else {
                $tasks = $tasks->where('tasks.status', '!=', '1');
            }
            if(array_key_exists('priority', $request)) {
                if($request['priority'] || $request['priority'] == '0') {
                    $tasks = $tasks->where('tasks.priority', $request['priority']);
                }
            }
            if(array_key_exists('users', $request)) {
                $val = $request['users'];
                $tasks = $tasks->where(function($query) use ($val) {
                    $query->where('tasks.assigned_to', $val)
                    ->orWhere('tasks.user', $val)
                    ->orWhereRaw('json_contains(tasks.observers, \'['.$val.']\')')
                    ->orWhereRaw('json_contains(tasks.participants, \'['.$val.']\')');
                });
            }
            if(array_key_exists('project', $request)) {
                $tasks = $tasks->where('tasks.project', $request['project']);
            }

            if(array_key_exists('_type', $request)) {
                if($request['_type'] == 'creator') {
                    $tasks = $tasks->where('tasks.user', $__id);
                } else if($request['_type'] == 'assisting') {
                    $tasks = $tasks->whereRaw('json_contains(tasks.participants, \'['.$__id.']\')');
                } else if($request['_type'] == 'ongoing') {
                    $tasks = $tasks->where('tasks.assigned_to', $__id);
                } else if($request['_type'] == 'following') {
                    $tasks = $tasks->whereRaw('json_contains(tasks.observers, \'['.$__id.']\')');
                }  
            }
        }

        if(Auth::user()->menuroles == 'admin') {
            $tasks = $tasks->withTrashed();
        } else {
            $tasks = $tasks->where(function($query) use ($__id) {
                $query->where('tasks.assigned_to', $__id)
                ->orWhere('tasks.user', $__id)
                ->orWhereRaw('json_contains(tasks.observers, \'['.$__id.']\')')
                ->orWhereRaw('json_contains(tasks.participants, \'['.$__id.']\')');
            });
        }


        $tasks = $tasks->groupBy('tasks.id')->orderBy('tasks.id', 'DESC');
        return $tasks;
    }

    /**
     * upload files.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    protected function upload_image($file, $_id) {
        $ext = $file->getClientOriginalExtension(); 
        $filename = $file->getClientOriginalName();
        $name = pathinfo($filename,PATHINFO_FILENAME);
       
        $name = $name.'-'.time().rand(100, 999).'.'.$ext;
        $path = "/images/tasks/".$_id;
        if(!File::isDirectory($path)){
            File::makeDirectory($path, 0777, true, true);
        }

        $destinationPath = public_path('/images/tasks/'.$_id);      
        $file->move($destinationPath, $name);

        return $path."/".$name;
    }

    /* Datatable controller.
     *
    */
    protected function generateDatatable($listing) {
        return DataTables::of($listing)
            // ->addIndexColumn()
            ->addColumn('responsible_person', function($row){  
                return $row->responsible_person;
            })
            ->addColumn('project', function($row){  
                return '<p class="redirect-project" style="cursor:pointer" data-id="'.$row->project_id.'">'.$row->project_name.'</p>';
            })
            ->addColumn('created_by', function($row){  
                return $row->created_by;
            })
            ->setRowAttr(['style' => function($row){
                    return $row->trashed() ? 'background-color: #f5c1c1;' : '';
                }
            ])
            ->rawColumns(['responsible_person', 'project', 'created_by'])
            ->make(true);
    }

    /**
     * tasks index function.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function index() {
        $vals = [];
        if(array_key_exists('project', $_GET)) {
            $vals['project'] = $_GET['project'];
        }
        if(array_key_exists('type', $_GET)) {
            $vals['_type'] = $_GET['type'];
        }

        if(array_key_exists('priority', $_GET)) {
            $vals['priority'] = $_GET['priority'];
        }
        if(array_key_exists('status', $_GET)) {
            $vals['status'] = $_GET['status'];
        }
        if(array_key_exists('users', $_GET)) {
            $vals['users'] = $_GET['users'];
        }

    	$tasks = $this->listTasks($vals)->get();
        return $this->generateDatatable($tasks);
    	
    }

    /**
     * Add new task.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function view($id) {
        $task = Task::leftJoin('users as a', 'tasks.user', '=', 'a.id')
            ->leftJoin('user_information as info', 'tasks.user', '=', 'info.user_id')
            ->leftJoin('users as b', 'tasks.assigned_to', '=', 'b.id')
            ->leftJoin('user_information as resp', 'tasks.assigned_to', '=', 'resp.user_id')
            ->leftJoin('folders', 'folders.project_id', '=', 'tasks.project')
            ->select('tasks.*', 'a.name as user_name', 'a.menuroles as user_role', 'b.menuroles as assignee_role', 'info.profile as user_profile', 'b.name as assignee_name', 'folders.id as folder_id', 'resp.profile as assignee_profile')
            ->where('tasks.id', $id)
            ->first();

        $project = '';
        if($task->project) {            
            $project = Project::where('id', $task->project)->first();
        }

        $current = User::leftJoin('user_information', 'users.id', '=', 'user_information.user_id')->select('users.id', 'users.name', 'user_information.profile')->where('users.id', Auth::user()->id)->first();

        $observers = array(); $participents = array();
        $obs = json_decode($task->observers);
        $part = json_decode($task->participants);

        if(count($obs) > 0) {
            foreach ($obs as $key => $value) {
                if($value) {
                    $user = User::leftJoin('user_information', 'users.id', '=', 'user_information.user_id')->select('users.id', 'users.name', 'user_information.profile', 'users.menuroles')->where('users.id', $value)->first();
                    if($user && $user->id) { 
                        $observers[$key]['name'] = $user->name;
                        $observers[$key]['profile'] = $user->profile;
                        $observers[$key]['role'] = $user->menuroles;
                    }
                }
            }
        }
        if(count($part) > 0) {
            foreach ($part as $k => $val) {
                if($val) {
                    $usr = User::leftJoin('user_information', 'users.id', '=', 'user_information.user_id')->select('users.id', 'users.name', 'user_information.profile', 'users.menuroles')->where('users.id', $val)->first();
                   
                    if($usr && $usr->id) { 
                        $participents[$k]['name'] = $usr->name;
                        $participents[$k]['profile'] = $usr->profile;
                        $participents[$k]['role'] = $usr->menuroles;
                    }
                }
            }
        }
        $users = User::select('id', 'name', 'name as text')->get();

        return response()->json([ 'status' => 'success', 'task' => $task, 'current' => $current, 'observers' => $observers, 'participents' => $participents, 'project' => $project, 'users' => $users ]);
    }

    /**
     * Add new task.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function add() {}

    /**
     * send chrome notifications.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    protected function notifyUsers($users, $body, $url) {
        $u_infor = DB::table('user_information')->where('user_id', Auth::user()->id)->select('thumbnail')->first();
        $tokens = [];
        foreach ($users as $key => $value) {
            if($value) {
                $user = User::select('id', 'device_token')->where('id', $value)->first();
                if($user) {
                    if($user->device_token) {
                        $tokens[] = $user->device_token; 
                    }
                } 
            }
        }
        if(count($tokens) > 0) { 
            User::send_notification($tokens, ucwords(Auth::user()->name), $body, $u_infor->thumbnail ?? null, $url);
        }

        return true;
    }

    /**
     * save the task.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function save(Request $request) {
    	$message = 'Something went known.'; $status = 'error';
    	$task = new Task;

        $notify = array_merge(json_decode($request['observer']),json_decode($request['participants']));
        $notify[] = $request['responsible'];
        $notify = array_unique(array_diff($notify, [Auth::user()->id]), SORT_REGULAR);

    	$task->name = $request['name'];
    	$task->created_at = $request['created_at'];
    	$task->deadline = ($request['deadline'])?$request['deadline']:null;
        $task->priority = $request['priority'];
    	$task->description = $request['description'];
    	$task->participants = $request['participants'];
    	$task->observers = $request['observer'];
    	$task->project = $request['project'];
    	$task->assigned_to = $request['responsible'];
        $task->extras = $request['extras'];
    	$task->user = Auth::user()->id; 

    	if($task->save()) {
    		$number = rand(10,100);
    		Task::where('id', $task->id)->update([ 'number' => $task->id.''.$number ]);
    		$message = 'Task Has been created successfully'; $status = 'success';

            $files = array();
            if(array_key_exists('files', $request->all())) {
                // foreach($request->file('files') as $key => $value) {
                //     $image = $this->upload_image($value, $task->id);
                //     $files[$key] = $image;
                // }
                Task::where('id', $task->id)->update([ 'files' => json_encode($request['files']) ]);
            }   
          
            $project = Project::find($request['project']);
            $path = "/files/".$project->name;
            Storage::makeDirectory($path,0777, true, true);
            $tasks = Task::where('project', $request['project'])->get();
            $response=null;
            $observer=[];
            $participant=[];
            if($tasks->count()) {    
               foreach ($tasks as $key => $value) {                   
                    $participants = json_decode($value->participants);
                    $observers = json_decode($value->observers);
                    foreach ($participants as $index => $parti) {
                        $participant[$key][$index] = $parti;
                    }
                    foreach ($observers as $index1 => $obser) {
                        $observer[$key][$index1] = $obser;
                    }
                    $assignedTo[$key] = $value->assigned_to;
                    $user[$key] = $value->user;
               }
                
               $observer=array_unique($this->array_flatten($observer));
               $participants=array_unique($this->array_flatten($participant));
               $assignedTo = array_unique($assignedTo);
               $user = array_unique($user);
               $response = array_merge ($observer,$participants,$assignedTo,$user);
               $response = array_unique($response);
              
               
            }
            $drive = $this->getTaskAssignUsers($request,$response);
            
            $driveFolder = DriveFolder::updateOrCreate(
                [
                    'project_id' => $request['project']
                ],
                [
                    'drive' => $drive,
                    'name' => $project->name,
                    'isFolder' => true,
                    'path' => $path,
                    'user_id' => Auth::id(),
                    
                ]
            );
            $driveFolder->link = 'drives/'.base64_encode($driveFolder->id);
            
            $driveFolder->save();
    	}
        
        $componentId = $task->id;
        $destinationUser[] = $task->user; $extras = [];

        if($task->observers && $task->participants) {
            $extras = array_merge(json_decode($task->observers), json_decode($task->participants));
        } elseif($task->observers || $task->participants) {
            $extras = json_decode($task->observers) || json_decode($task->participants);
        } 

        $destinationUser = array_merge($destinationUser, $extras);
        $nextKey = array_key_last($destinationUser)+1;
        $destinationUser[$nextKey] = $task->assigned_to; 

        $destinationUser = array_unique(array_diff($destinationUser, [Auth::user()->id]), SORT_REGULAR);
        $status="Added a new task";
        $this->UserNotification($destinationUser,$task->id,$status);
        $url = url('/tasks').'/'.base64_encode($request['id']);
        $this->notifyUsers($destinationUser, 'Created new tasks '.$request['name'].' '.$url, $url);

    	return response()->json([ 'status' => $status, 'message' => $message ]);

    }
     /**
     * User  Notification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    private function UserNotification($users,$taskId,$status)
    {
        $task = Task::find($taskId);
        $text = $status." <a class='notification-task' data-id='".$task->id."'>[#".$task->number."]".$task->name."(".$task->projectModel->name.")</a>";
        foreach ($users as $key => $user) {
           $userNotification =  New UserNotification;
           $userNotification->user_id = $user;
           $userNotification->text = $text;
           $userNotification->read = "N";
           $userNotification->type = "sidebar";
           $userNotification->created_by = Auth::id();
           $userNotification->save();
        }
        
    }
    /**
     * edit the task.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function edit($id) {
    	$task = Task::where('id', $id)->first();
    	$users = User::select('id', 'name')->orderBy('name')->get();
    	$projects = Project::select('id', 'name', 'proposal_id')->orderBy('name')->get();

    	return response()->json([ 'status' => 'success', 'projects' => $projects, 'users' => $users, 'task' => $task ]);
    }

    /**
     * update the task.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function update(Request $request) {
        $files = array(); $k = 0;
        if(array_key_exists('old_files', $request->all())) {
            foreach($request['old_files'] as $old) {
                $files[$k] = $old; $k++;
            }
        }
        if(array_key_exists('files', $request->all())) {
            foreach($request['files'] as $key => $value) {
                // $image = $this->upload_image($value, $request['id']);
                $files[$k] = $value; $k++;
            }
        }

    	Task::where('id', $request['id'])->update([
    		"name" => $request['name'],
    		"deadline" => $request['deadline'],
    		"description" => $request['description'],
    		"participants" => $request['participants'],
    		"observers" => $request['observer'],
    		"project" => $request['project'],
            "priority" => $request['priority'],
    		"assigned_to" => $request['responsible'],
            "extras" => $request['extras'],
            "files" => json_encode($files),
    		"status" => $request['status']
    	]);


        $notify = array_merge(json_decode($request['observer']),json_decode($request['participants']));
        $notify[] = $request['responsible'];
        $notify = array_unique(array_diff($notify, [Auth::user()->id]), SORT_REGULAR);
        $url = url('/tasks').'/'.base64_encode($request['id']);

        $task = Task::find($request['id']);
            $project = Project::find($request['project']);
            $path = "/files/".$project->name;
            Storage::makeDirectory($path,0777, true, true);
            $tasks = Task::where('project', $request['project'])->whereNotIn('id', [$request['id']])->get();
            $response=null;
            $observer = [];
            $participant = [];
            if($tasks->count()) {    
               foreach ($tasks as $key => $value) {                   
                    $participants = json_decode($value->participants);
                    $observers = json_decode($value->observers);
                    foreach ($participants as $index => $parti) {
                        $participant[$key][$index] = $parti;
                    }
                    foreach ($observers as $index1 => $obser) {
                        $observer[$key][$index1] = $obser;
                    }
                    $assignedTo[$key] = $value->assigned_to;
                    $user[$key] = $value->user;
               }
                
               $observer=array_unique($this->array_flatten($observer));
               $participants=array_unique($this->array_flatten($participant));
               $assignedTo = array_unique($assignedTo);
               $user = array_unique($user);
               $response = array_merge ($observer,$participants,$assignedTo,$user);
               $response = array_unique($response);
              
               
            }
            $drive = $this->getTaskAssignUsers($request,$response);

            $driveFolder = DriveFolder::updateOrCreate(
                [
                    'project_id' => $request['project']
                ],
                [
                    'drive' => $drive,
                    'name' => $project->name,
                    'isFolder' => true,
                    'path' => $path,
                    'user_id' => Auth::id()
                ]
            );
          $driveFolder->link = 'drives/'.base64_encode($driveFolder->id);
          $driveFolder->save();

        $componentId = $task->id;
        $destinationUser[] = $task->user; $extras = [];

        if($task->observers && $task->participants) {
            $extras = array_merge(json_decode($task->observers), json_decode($task->participants));
        } elseif($task->observers || $task->participants) {
            $extras = json_decode($task->observers) || json_decode($task->participants);
        } 

        $destinationUser = array_merge($destinationUser, $extras);
        $nextKey = array_key_last($destinationUser)+1;
        $destinationUser[$nextKey] = $task->assigned_to; 

        $destinationUser = array_unique(array_diff($destinationUser, [Auth::user()->id]), SORT_REGULAR);

        $action="update";
        $status="Updated the task";
        $this->UserNotification($destinationUser,$task->id,$status);
        $this->notifyUsers($destinationUser, 'Updated task '.$request['name'].' '.$url, $url);


    	$message = 'Task Has been updated successfully'; $status = 'success';
    	return response()->json([ 'status' => $status, 'message' => $message, 'files' => $task->files ]);
    }

    /**
     * delete the task.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function delete($id) {
    	Task::where('id', $id)->delete();
    	return response()->json([ 'status' => 'success' ]);
    }

    /**
     * Filter tasks.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function filterTasks(Request $request) {
        $tasks = $this->listTasks($request->all())->get();

        return response()->json([ 'status' => 'success', 'tasks' => $tasks ]);
    }




    public function testing() {
        $projects = Project::get();
        foreach ($projects as $key => $value) {
            $tasks = Task::leftJoin('users as from', 'tasks.user', '=', 'from.id')
            ->leftJoin('users as to', 'tasks.assigned_to', '=', 'to.id')
            ->select('tasks.*', 'from.name as user_name', 'to.name as responsible_person')
            ->where('tasks.project', $value->id);

            $tasks = $tasks->orderBy('id', 'DESC')->get();
            $value['tasks'] = $tasks;
        }

        return response()->json([ 'status' => 'dfg', 'df' => $projects ]);
    }
    private function getTaskAssignUsers($request, $response=null)
    {       
        $obervers = json_decode($request->observer);
        $participants = json_decode($request->participants);
        $result =  array_merge($obervers, $participants);
        $newArray = [];
        $newArray[0] = $request['responsible'];
        $newArray[1] = Auth::id(); 
        $result = array_merge($result, $newArray);  
        $users = User::where('menuroles', 'admin')->pluck('id')->toArray();
        $result = array_unique(array_merge($result, $users));        
        if($response) {
            $result= array_unique(array_merge($result, $response));
        }

        $result = array_map(function($value) { return '"drive_'.$value.'"'; }, $result); 
        $unique = array_unique($result, SORT_REGULAR); 
        $duplicated=array(); 
        foreach($unique as $k=>$v) { 
            if( ($kt=array_search($v,$unique))!==false and $k!=$kt ) 
            { 
            unset($unique[$kt]); $duplicated[]=$v;
             } 
        }
        $result = "[";
        $result .= implode(",",$unique);
        $result .= "]";

        return $result;
    }

    /**
     * logo.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function getLogo() {
        $logo = Setting::where('name', 'logo')->first();
        return response()->json([ 'status' => 'success', 'logo' => $logo->value ]);
    }

    /**
     * timezone.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function timezone() {
        $timezone = Setting::where('name', 'timezone')->first();
        return response()->json([ 'status' => 'success', 'timezone' => $timezone->value ]);
    }

    /**
     * save notification token.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function save_token(Request $request) {
        $user = User::findOrFail($request->user_id);
        $user->device_token = $request->device_token;
        $user->save();

    }

    /**
     * get project name.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function projectName($id) {
        $name = Project::where('id', $id)->select('projects.name')->first();
        return response()->json($name->name);
    }

    /**
     * Bulk actions.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function bulkUpdate(Request $request) {

        $ids = explode(',', $request['ids']); $status = 'success';

        if($request['action'] == 'delete') {
            foreach ($ids as $key => $value) {
                $task = Task::find($value);
                if(Auth::user()->menuroles == 'admin' || $task->user == Auth::user()->id) {
                    Task::where('id', $value)->delete();
                } else {
                    $status = 'warning';
                }
            }
        } else {
            foreach ($ids as $key => $value) {
                Task::where('id', $value)->update([ 'status' => $request['action'] ]);
            }
        }

        return response()->json([ 'status' => $status ]);
    }
    private function array_flatten($array) { 
      if (!is_array($array)) { 
        return FALSE; 
      } 
      $result = array(); 
      foreach ($array as $key => $value) { 
        if (is_array($value)) { 
          $result = array_merge($result, array_flatten($value)); 
        } 
        else { 
          $result[$key] = $value; 
        } 
      } 
      return $result; 
    } 
}
