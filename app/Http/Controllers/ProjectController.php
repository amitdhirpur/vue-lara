<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use Illuminate\Support\Facades\Validator;
use App\Repositories\Resource\ResourceRepository;
use App\User;
use App\Models\Log;
use Auth;
use App\DriveFolder;
use App\Drive;
use File, Storage, DB, DataTables;

class ProjectController extends Controller
{ 
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $model;

    public function __construct(Project $project)
    {
        $this->middleware('auth:api');
        $this->model = new ResourceRepository($project);
    }

    /**
     * Datatable controller.
     *
    */
    protected function generateDatatable($listing) {
        return DataTables::of($listing)
            // ->addIndexColumn()
            ->addColumn('client', function($row){  
                return $row->client;
            })
            ->addColumn('url', function($row){  
                return $row->url;
            })
            ->addColumn('portal', function($row){  
                return $row->portal;
            })
            ->addColumn('profile', function($row){  
                return $row->profile;
            }) 
            ->setRowAttr(['style' => function($row){
                    return $row->trashed() ? 'background-color: #f5c1c1;' : '';
                }
            ])
            ->rawColumns(['client', 'url', 'portal', 'profile'])
            ->make(true);
    }
     /**
     * Datatable controller.
     *
    */
    protected function generateTrashedDatatable($listing) {
        return DataTables::of($listing)
            // ->addIndexColumn()
            ->addColumn('client', function($row){  
                return $row->client;
            })
            ->addColumn('url', function($row){  
                return $row->url;
            })
            ->addColumn('portal', function($row){  
                return $row->portal;
            })
            ->addColumn('profile', function($row){  
                return $row->profile;
            })             
            ->rawColumns(['client', 'url', 'portal', 'profile'])
            ->make(true);
    }
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::taksRelatedProjects(Auth::id());
        return $this->generateDatatable($projects);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $project = $this->model->show($id);
        $project = Project::leftJoin('folders', 'folders.project_id', '=', 'projects.id')
                    ->select('projects.*','folders.id as folder_id','folders.name as folder_name')
                      ->where('projects.id', '=', $id)
                    ->first();
        return response()->json( $project );
    }
/**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        return response()->json( ['status' => 'success'] );  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'name'       => 'required|min:1|max:256',
            'url'      => 'required',
            'client' => 'required',
            'portal'   => 'required',
            'profile'       => 'required'
        ]);
          if ($validate->fails()){
            return response()->json([
                'status' => 'error',
                'errors' => $validate->errors()
            ], 422);
        }
        $name="";
        if($request->client) {
            $name .= $request->client.'-';
        }
        if($request->name) {
            $name .= $request->name.'-';
        }
        if($request->portal) {
            $name .= $request->portal.'-';
        }
        if($request->profile) {
            $name .= $request->profile;
        }
        $request->request->add(['name' =>$name]);
        $request->request->add(['user_id' => Auth::id()]);
        $project =  $this->model->create($request->only($this->model->getModel()->fillable));
        $path = "/files/".$project->name;
        $users = User::where('menuroles', 'admin')->pluck('id')->toArray();
        $users[end($users)+1] = Auth::id();
        $users = array_unique($users);
        $drive = $this->createJson(array_unique($users));
        $path = "/files/".$name;
        Storage::makeDirectory($path,0777, true, true);
        $driveFolder = DriveFolder::updateOrCreate(
            [
                'project_id' => $project->id
            ],
            [
                'drive' => $drive,
                'name' => $project->name,
                'isFolder' => true,
                'path' => $path,
                'user_id' => Auth::id()
            ]
        );
        
        $driveFolder->link = 'drives/'.base64_encode($driveFolder->id);
        $driveFolder->save();
          
       

        return response()->json(['status' => 'success'], 200);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = $this->model->show($id);
        return response()->json( $project );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validatedData = Validator::make($request->all(), [
            'name'       => 'required|min:1|max:256',
            'url'      => 'required',
            'client' => 'required',
            'portal'   => 'required',
            'profile'       => 'required'
        ]);
        if ($validatedData->fails()){
            return response()->json([
                'status' => 'error',
                'errors' => $validatedData->errors()
            ], 422);
        }
        $project = Project::find($id);
        $old_project_name = $project->name;
        $old_project_path = "/files/".$project->name;

        if($project->count() && $project->access_detail != $request->input('access_detail')){
            $log = New Log;
            $log->user_id       =  Auth::id();
            $log->component     = 'project';
            $log->component_id  =  $id;
            $log->description   =  $request->input('description');

            $log->save();
        }
        $project->name       = $request->input('name');
        $project->url      = $request->input('url');
        $project->client      = $request->input('client');
        $project->portal      = $request->input('portal');
        $project->description      = $request->input('description');
        $project->profile      = $request->input('profile');
        $project->access_detail      = $request->input('access_detail');
        $project->save();

        $path = "/files/".$project->name;

       //  echo $project_name; die;
        if(Storage::exists($old_project_path)) {
            if(!Storage::exists($path)){
              Storage::rename($old_project_path, $path);
            } 
            $folders =  DriveFolder::where("project_id",$id)->first();
            $rootFolder = DriveFolder::findOrFail($folders->id);
            $folder = DB::select("select id, name,parent_id from (select * from folders order by parent_id, id) products_sorted, (select @pv := '".$folders->id."') initialisation where find_in_set(parent_id, @pv)and length(@pv := concat(@pv, ',', id))");

            foreach ($folder as $key => $value) {
               $DriveFolder =  DriveFolder::where("id",$value->id);
               $get_drivefolders = $DriveFolder->first();
               $new_path = str_replace($old_project_name, $project->name, $get_drivefolders->path);
               $DriveFolder->update(["path"=>$new_path]);
            }
           
        }
        
        //
        $driveFolder = DriveFolder::updateOrCreate(
            [
                'project_id' => $project->id
            ],
            [
                'name' => $project->name,
                'isFolder' => true,
                'path' => $path,
                'user_id' => Auth::id()
            ]
        );
        
        $driveFolder->link = 'drives/'.base64_encode($driveFolder->id);
        $driveFolder->save();

        //$request->session()->flash('message', 'Successfully updated user');
        return response()->json( ['status' => 'success'] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $this->model->delete($id);
      
        return response()->json( ['status' => 'success'] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function listProjects() {
       $users = User::select('id', 'name')->where('is_active', '1')->orderBy('name')->get();
       $projects = Project::select('id', 'name', 'proposal_id')->orderBy('name')->get();

        return response()->json([ 'status' => 'success', 'projects' => $projects, 'users' => $users ]);
    }
    public function saveAccessDetail(Request $request)
    {
        $id = $request->input('id');
        $accessDetail = $request->input('access_detail');

        $project = Project::find($id);

        if($project->count() && $project->access_detail != $accessDetail){
            $log = New Log;
            $log->user_id       =  Auth::id();
            $log->component     = 'project';
            $log->component_id  =  $id;
            $log->description   =  $project->description;

            $log->save();
        }
        $project->update(['access_detail' => $accessDetail]);

        return response()->json($project);
    }
    public function archiveProjects()
    {
       $projects = Project::taksRelatedArchiveProjects(Auth::id());
        return $this->generateTrashedDatatable($projects);
    }
    public function archiveRestore(Request $request)
    {
      $id = $request->id;
      $project = Project::withTrashed()->find($id);
      $project->restore();
      return response()->json('success');
    }
    public function createJson($users)
    {
        $count = count($users);
        $destUser="";
        if($count) {
            $i=1;
            $destUser="[";
            foreach ($users as $key => $value) {
            if($count == $i) {
            $destUser .='"drive_'.$value.'"';
            } else {
            $destUser .='"drive_'.$value.'",';
            }

            $i++;
            }
            $destUser.="]";
        }
        
        return $destUser;
    }
}