<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;

class UploadController extends Controller
{
    /**
     * upload files.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    protected function upload_image($file, $_id) {
        $ext = $file->getClientOriginalExtension(); 
        $filename = $file->getClientOriginalName();
        $name = pathinfo($filename,PATHINFO_FILENAME);
       
        $name = $name.'-'.time().rand(100, 999).'.'.$ext;
        $path = "/images/files";
        if(!File::isDirectory($path)){
            File::makeDirectory($path, 0777, true, true);
        }

        $destinationPath = public_path('/images/files');      
        $file->move($destinationPath, $name);

        return $path."/".$name;
    }

    /**
     * tasks index function.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function index(Request $request) {
    	
        $image = $this->upload_image($file);
    	return response()->json($image);
    }
}
