<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Project;
use App\DriveFolder;
use App\Drive;
use Illuminate\Support\Facades\Validator;
use App\models\Log;
use Auth;
use File, Storage;
use ZipArchive;
use Zip;
class DrivesController extends Controller
{ 
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public $imageType;
    public $videoType;
    public $incrmnt=0;
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->imageType = ["png","jpeg","jpg","gif","tiff","bmp"];
        $this->videoType = ["mp4"];
    }
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $driveFoldersAndFiles = $this->driveFoldersAndFiles();

        return response()->json(['driveFolders' => $driveFoldersAndFiles->driveFolders, 'test' => $driveFoldersAndFiles->lightboxFiles]);
    }
     public function add(Request $request)
    {
         $filePath = [];
         $folderPath='/files';
         if($request->parent) {
         	$parent = base64_decode($request->parent);
         	$folderPath = DriveFolder::where('id', $parent)->where('isFolder', 'true')->value('path');
         }
         if ($request->hasFile('files')) {
            $files = $request->file('files');
            foreach($files as $key => $file){
                $ext = $file->getClientOriginalExtension();
                $images = $this->upload_image($file,$ext,$folderPath);
                $filePath[$key]['ext'] = $ext;
            
                $driveFolder = New DriveFolder;
                $driveFolder->parent_id = $parent ?? null;
                $driveFolder->user_id = Auth::id();
                $driveFolder->path = $images->path;
                $driveFolder->name = $images->name;
                $driveFolder->type = $ext;
                $driveFolder->drive = json_encode([$value[0]='drive_'.Auth::id()]);
                $driveFolder->isFile = $request->isFile ?? false;  

                $driveFolder->save();

            }
                  
        }

		 $driveFoldersAndFiles = $this->driveFoldersAndFiles($request);

        return response()->json(['driveFolders' => $driveFoldersAndFiles->driveFolders, 'test' => $driveFoldersAndFiles->lightboxFiles]);
    }
    public function addFolder(Request $request)
    {
        $filePath = [];
        $folderPath='/files';
        if($request->parent){
        	$parent = base64_decode($request->parent);
       		$folderPath = DriveFolder::where('id', $parent)->where('isFolder', 'true')->value('path');
        }
        if($request->_folder) {
        	$path = $folderPath.'/'.$request->_folder;
          if(!Storage::exists($path)){
    	        Storage::makeDirectory($path,0777, true, true);
    	        $driveFolder = New DriveFolder;
    	        $driveFolder->parent_id = $parent ?? null;
    	        $driveFolder->path = $path;
                $driveFolder->user_id = Auth::id();
    	        $driveFolder->name = $request->_folder;
    	        $driveFolder->drive = json_encode([$value[0]='drive_'.Auth::id()]);
    	        $driveFolder->isFolder = $request->is_folder ?? false;  

    	        
    	        if($driveFolder->save()) {
    	            $driveFolder->link = $request->current_path.'/'.base64_encode($driveFolder->id);
    	            $driveFolder->save();
    	        }
          } else {
            return response()->json(['errors'=> 'Folder Already exist!']);
          }
        }
        

        $driveFoldersAndFiles = $this->driveFoldersAndFiles($request);

        return response()->json(['driveFolders' => $driveFoldersAndFiles->driveFolders, 'test' => $driveFoldersAndFiles->lightboxFiles]);
    }
    public function deriveFiles($id)
    {
        $files = Drive::where('id', $id)->first();
            
        return response()->json($files);
    }
    public function getFolderAndFiles($value)
    {
        $folders = explode('/', $value);
        $parentId = base64_decode(end($folders));
        $parentFolder = DriveFolder::where('id', $parentId)->first();
        $drive = Drive::where('name', 'drive_'.Auth::id())->first();
        $query = DriveFolder::where('parent_id', $parentId);
        $driveFolders = $query->orderBy('name')->get()->each(function($folders, $index) {
            if(in_array($folders->type, $this->imageType) || in_array($folders->type, $this->videoType)) {
              $folders->keyVal =  $this->incrmnt; 
              $this->incrmnt++;
            } 
            
        });
        $driveFoldersAndFiles = $this->getDriveFolders($driveFolders);

        return response()->json(['driveFolders' => $driveFoldersAndFiles->driveFolders, 'test' => $driveFoldersAndFiles->lightboxFiles, 'parentFolder' => $parentFolder]);
       
    }   
      // upload  image 
    public function upload_image($file,$ext,$folderPath) {
        $filename = $file->getClientOriginalName();
        $name = pathinfo($filename,PATHINFO_FILENAME);
        
        $name = $name.time().'.'.$ext;

        $destinationPath = public_path($folderPath);        
        $file->move($destinationPath, $name);

        $path = $folderPath."/".$name;
        $return = (object)[];
        $return->path = $path;
        $return->name = $name;

        return $return;
    }
    public function delete(Request $request)
    {
        $id=$request->id;
        $rootFolder = DriveFolder::findOrFail($id);
        $folder = DB::select("select id, name,parent_id from (select * from folders order by parent_id, id) products_sorted, (select @pv := '".$id."') initialisation where find_in_set(parent_id, @pv)and length(@pv := concat(@pv, ',', id))");

        if($folder) {
            $ids = array_column($folder, 'id');
            DriveFolder::destroy($ids);
            Storage::deleteDirectory($rootFolder->path);
            $rootFolder->delete();
        } else {
             if($rootFolder->isFolder == 'true') {
                Storage::deleteDirectory($rootFolder->path);
             } else {   
                Storage::delete($rootFolder->path);
             }
            $rootFolder->delete();
        }

        $driveFoldersAndFiles = $this->driveFoldersAndFiles($request);

        return response()->json(['driveFolders' => $driveFoldersAndFiles->driveFolders, 'test' => $driveFoldersAndFiles->lightboxFiles]);
    }
    public function downloadZipFolder(Request $request)
    {
        $sourcePath = public_path($request->path);
        $destinationPath = '/downloadArchive/'.$request->name.'.zip';
        $outZipPath = public_path($destinationPath);
     
        if (!extension_loaded('zip') || !file_exists($sourcePath)) {return false;}

        $zip = new ZipArchive();
        if (!$zip->open($outZipPath, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE)) {return false;}

        $sourcePath = str_replace('\\', '/', realpath($sourcePath));

        if (is_dir($sourcePath) === true) {
            $files = new  \RecursiveIteratorIterator(new  \RecursiveDirectoryIterator($sourcePath),  \RecursiveIteratorIterator::SELF_FIRST);

            foreach ($files as $file) {
                $file = str_replace('\\', '/', $file);

                // Ignore "." and ".." folders
                if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
                    continue;

                $file = realpath($file);

                if (is_dir($file) === true) {
                    $zip->addEmptyDir(str_replace($sourcePath . '/', '', $file . '/'));
                } else if (is_file($file) === true) {
                    $zip->addFromString(str_replace($sourcePath . '/', '', $file), file_get_contents($file));
                }
            }
        } else if (is_file($sourcePath) === true) {
            $zip->addFromString(basename($sourcePath), file_get_contents($sourcePath));
        }

        $zip->close();
        if(file_exists($outZipPath)) {
          return response($destinationPath);            
        }
        return response()->json(['error' => 'Folder empty']);
        
    }
    private function driveFoldersAndFiles($request=null)
    {       
        $drive = Drive::where('name', 'drive_'.Auth::id())->first();
        $query = DriveFolder::query();
        if(isset($request->parent)) {
            $parent = base64_decode($request->parent);
            $query->where('parent_id',$parent);
        } else {
           $query->whereRaw('json_contains(drive, \'["' . $drive->name . '"]\')')->whereNull('parent_id');
        }

        $driveFolders = $query->orderBy('name')->get()->each(function($folders, $index) {
            if(in_array($folders->type, $this->imageType)) {
              $folders->keyVal =  $this->incrmnt; 
              $this->incrmnt++;
            } 
            
        });;

        $driveFolders = $this->getDriveFolders($driveFolders);        

        return $driveFolders;
    }
     //
    private function getDriveFolders($driveFolders)
    {
         $lightboxFiles=[];
         $i=0;
        foreach ($driveFolders as $key => $driveFolder) {
            if(in_array($driveFolder->type, $this->imageType)) {
                $lightboxFiles[$i]['thumb'] = $driveFolder->path;
                $lightboxFiles[$i]['src'] = $driveFolder->path;
                $i++;
            }
            if(in_array($driveFolder->type, $this->videoType)) {
                $lightboxFiles[$i]['thumb'] = $driveFolder->path;
                $lightboxFiles[$i]['sources']['0']['src'] = $driveFolder->path;
                $lightboxFiles[$i]['sources']['0']['type'] =  'video/mp4';
                $lightboxFiles[$i]['type'] = 'video';
                $lightboxFiles[$i]['width'] = '800';
                $lightboxFiles[$i]['height'] = '600';
                $lightboxFiles[$i]['autoplay'] = 'true';
                $i++;
              }
            
        }
        $driveFoldersAndFiles = (object)[];
        $driveFoldersAndFiles->driveFolders = $driveFolders;
        $driveFoldersAndFiles->lightboxFiles = $lightboxFiles;

        return $driveFoldersAndFiles;
    }
    public function searchDrive(Request $request)
    {
        $drive = Drive::where('name', 'drive_'.Auth::id())->first();
        $query = DriveFolder::query();
        if($request->search) {
           $query->where('name', 'LIKE', "%".$request->search."%");
        }
       
        if(isset($request->parent)) {
            $parent = base64_decode($request->parent);
            $query->where('parent_id',$parent);
        } else {
           $query->whereRaw('json_contains(drive, \'["' . $drive->name . '"]\')')->whereNull('parent_id');
        }

        $driveFolders = $query->orderBy('name')->get()->each(function($folders, $index) {
            if(in_array($folders->type, $this->imageType)) {
              $folders->keyVal =  $this->incrmnt; 
              $this->incrmnt++;
            } 
            
        });;

        $driveFolders = $this->getDriveFolders($driveFolders);  

      return response()->json(['driveFolders' => $driveFolders->driveFolders, 'test' => $driveFolders->lightboxFiles]);
    }
     public function renameFolderOrFile(Request $request)
    {
        $name = $request->renameFolderName;
        $oldName = $request->oldFolderName;
        $folder =  DriveFolder::where("id",$request->id)->first();
        $parentFolder =  DriveFolder::where("id",$folder->parent_id)->first();       
        $old_project_path = $folder->path;
        $old_project_name = $folder->name;
       if($request->isFolder == 'true') {
            $folderName =  DriveFolder::where("parent_id",$folder->parent_id)->where('isFolder','true')
                            ->where('name','LIKE', $name)
                            ->first();
            if($folderName) {
              return response()->json(['errors'=> 'Folder Already exist!']);
            }              
            $new_project_path = $parentFolder->path.'/'.$name;
            if(!Storage::exists($new_project_path)){
                Storage::rename($old_project_path, $new_project_path);
                $folders = DB::select("select id, name,parent_id from (select * from folders order by parent_id, id) products_sorted, (select @pv := '".$request->id."') initialisation where find_in_set(parent_id, @pv)and length(@pv := concat(@pv, ',', id))");
                foreach ($folders as $key => $value) {
                   $DriveFolder =  DriveFolder::where("id",$value->id);
                   $get_drivefolders = $DriveFolder->first();
                   $new_path = str_replace($old_project_name, $name, $get_drivefolders->path);
                   $DriveFolder->update(["path"=>$new_path]);
                }

               $folder->path=$new_project_path;
            } else {
              return response()->json(['errors'=> 'Folder Already exist!']);
        }
      } else {                    
             $ext = pathinfo($oldName, PATHINFO_EXTENSION);
             $newext = pathinfo($name, PATHINFO_EXTENSION);
             if($newext) {
              $name = $name;
             }else {
              $name = $name.'.'.$ext;
             }
             $fileName = DriveFolder::where("parent_id",$folder->parent_id)->where('isFile','true')
                          ->where('name','LIKE', $name)
                          ->first();
            if($fileName) {
                 return response()->json(['errors'=> 'Folder Already exist!']);
            }    
             $new_project_path = $parentFolder->path.'/'.$name;
         if(Storage::exists($old_project_path) && !Storage::exists($new_project_path)){            
             Storage::move($old_project_path, $new_project_path);
             $folder->path=$new_project_path;
           } else {
             if(Storage::exists($new_project_path)){
                 return response()->json(['errors'=> 'File Already exist!']);
             } else {
               return response()->json(['errors'=> 'File Not exist!']);
             }
           }            
      }
      $folder->name=$name;     
      $folder->save();

      $driveFoldersAndFiles = $this->driveFoldersAndFiles($request);

      return response()->json(['driveFolders' => $driveFoldersAndFiles->driveFolders, 'test' => $driveFoldersAndFiles->lightboxFiles]);
    }
  //   private  function folderToZip($folder, &$zipFile, $exclusiveLength) {
  //       $handle = opendir($folder);
  //       while (false !== $f = readdir($handle)) {
  //         if ($f != '.' && $f != '..') {
  //           $filePath = "$folder/$f";
  //           // Remove prefix from file path before add to zip.
  //           $localPath = substr($filePath, $exclusiveLength);
  //           if (is_file($filePath)) {
  //             $zipFile->addFile($filePath, $localPath);
  //           } elseif (is_dir($filePath)) {
  //             // Add sub-directory.
  //             $zipFile->addEmptyDir($localPath);
                
  //           }
  //         }
  //       }
  //       closedir($handle);
  // }
}