<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Project;
use Illuminate\Support\Facades\Validator;
use App\models\Log;
use Auth;

class LogsController extends Controller
{ 
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logs = DB::table('logs')
            ->leftJoin('users', 'users.id', '=', 'logs.user_id')
            ->leftJoin('projects', 'projects.id', '=', 'logs.component_id')
            ->select('logs.id', 'users.name as user', 'logs.component', 'logs.description', 'logs.created_at', 'projects.name as project_name','projects.id as project_id')
            ->orderBy('logs.id', 'DESC')
            ->get();

        return response()->json($logs);

    }
}