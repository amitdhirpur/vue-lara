<?php

namespace App\Http\Controllers;
use App\User;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{ 
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public $date_format;
    public function __construct()
    {
        $date = Setting::where("name","date_format")->first();
        $time = Setting::where("name","time_format")->first();
        $this->date_format = $date->value.' '.$time->value;
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }
  
    /**
     * Register new user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request){
        $validate = Validator::make($request->all(), [
            'name'      => 'required',
            'email'     => 'required|email|unique:users',
            'password'  => 'required|min:4|confirmed',
        ]);        
        if ($validate->fails()){
            return response()->json([
                'status' => 'error',
                'errors' => $validate->errors()
            ], 422);
        }        
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->status = 'Active';
        $user->save();       
        return response()->json(['status' => 'success'], 200);
    } 

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);
        

        $user = User::where("email",$credentials["email"]);
        if($user->count() < 1){

           return response()->json(['error' => 'User does not exist']);

        }else{

            if($user->first()->is_active != "1"){
              return response()->json(['error' => 'Account deactivated!']);
            }else{

                if (! $token = auth()->attempt($credentials)) {
                    return response()->json(['error' => 'Please enter valid username and password']);
                }

            }   

        }

        return $this->respondWithToken($token);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $user =  User::find(Auth::user()->id);
        $user->is_logged = null;
        $user->save();
        // User::where('id', Auth::user()->id )->update([ 'device_token' => null ]);
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        $type = false;
        if(Auth::user()->menuroles == 'admin') { $type = true; }
        return response()->json([
            'access_token' => $token,
            'user' => Auth::user()->id,
            'type' => $type,
            'date_format' => $this->date_format,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}