<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\DailyReport;
use App\User;
use DateTime, DataTables;
class DailyReportController extends Controller
{ 
	/**
     	* Create a new controller instance.
     	*
     	* @return void
     	*/
	/*
	*get reports
	*/
	/**
     	* Datatable controller.
     	*
    */
    protected function generateDatatable($listing) {
        return DataTables::of($listing)
            // ->addIndexColumn()
            ->addColumn('name', function($row){  
                return $row->name;
            })
            ->rawColumns(['name', 'report', 'report_date'])
            ->make(true);
    }

	public function index()
    {
    	$reports = DailyReport::leftJoin('users', 'daily_reports.user_id', '=', 'users.id')
    		->select('daily_reports.*', 'users.name');

    	if($_GET) {
    		if(array_key_exists('date', $_GET)) {
    			$reports = $reports->where('daily_reports.created_at','LIKE', $_GET['date']. '%');
    		}
	      	if(array_key_exists('user', $_GET)){
	         	$reports = $reports->where('daily_reports.user_id', $_GET['user']);
	      	}
    	}

    	$reports = $reports->latest('daily_reports.created_at')->get();

      	return $this->generateDatatable($reports);
    }

    public function listUsers() {
    	$users = [];
      	if(Auth::user()->menuroles == 'admin') {
         	$users = User::where('menuroles', '!=', 'admin')->select('id', 'name')->get();
      	}
      	return response()->json($users);
    }
    /*
		*save reports
	*/
    public function save(Request $request)
    {
    	$currentDateTime = new DateTime("now");
    	$report = DailyReport::where('user_id',  Auth::id())->where('created_at', 'LIKE', $currentDateTime->format("Y-m-d"). '%')->first();
    	$date = is_null($report) ? $currentDateTime : $report->created_at;
    	DailyReport::updateOrCreate(
   			[
		    	'user_id' => Auth::id(),
		    	'created_at' => $date
		    ],
		    [
		        'report' => $request->report
		    ]
		);
        $url = url('/reports');

        $users = User::where('menuroles','admin')->get();
        $tokens = [];
       	foreach ($users as $key => $user) {
            if($user && $user->device_token) {
                $tokens[] = $user->device_token;                   
            }
        }
      	$this->notifyUsers($tokens, 'Daily Status Report '.$url, $url);

	 	return response()->json([ 'status' => 'success']);
    }

    /*
		*user report
	*/
    public function report()
    {
    	$currentDateTime = new DateTime("now");
    	$report = DailyReport::where('user_id',  Auth::id())->where('created_at', 'LIKE', $currentDateTime->format("Y-m-d"). '%')->first();

    	return response()->json($report);
    }

     /*
    *Daily report
    */
    public function dailyReport($id)
    {
        $currentDateTime = new DateTime("now");
        $report = DailyReport::leftJoin('users', 'daily_reports.user_id', '=', 'users.id')
          	->leftJoin('user_information', 'user_information.user_id', '=', 'users.id')
          	->where('daily_reports.id',  $id)
            ->select('daily_reports.*', 'users.name','user_information.profile')->first();
        return response()->json($report);
    }

    protected function notifyUsers($tokens, $body, $url) {
        
        $u_infor = DB::table('user_information')->where('user_id', Auth::user()->id)->select('thumbnail')->first();
      
        if(count($tokens) > 0) { 
            User::send_notification($tokens, ucwords(Auth::user()->name), $body, $u_infor->thumbnail ?? null, $url);
        }

        return true;
    }
}