<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\UserPosition;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;

class PositionsController extends Controller
{

    /*
        TO REMOVE

    public function test(){
        //JWTAuth::toUser($token);

        $user = auth()->user();
        return response()->json( $user );
    }
    */

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $positions = DB::table('user_positions')       
        ->orderBy('id', 'asc')
        ->get();
        return response()->json( $positions );
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        return response()->json( ['status' => 'success'] );  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:1|max:128'
        ]);
        $userPosition = new UserPosition();
        $userPosition->name = $request->input('name');
        $userPosition->save();
        //$request->session()->flash('message', 'Successfully created role');
        return response()->json( ['status' => 'success'] );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     */
    public function show($id)
    {
        $userPosition = UserPosition::where('id', '=', $id)->first();
        return response()->json( array('name' => $userPosition->name) );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function edit($id)
    {
        $userPosition = UserPosition::where('id', '=', $id)->first();
        return response()->json( array(
            'id' => $userPosition->id,
            'name' => $userPosition->name
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|min:1|max:128'
        ]);
        $userPosition = UserPosition::where('id', '=', $id)->first();
        $userPosition->name = $request->input('name');
        $userPosition->save();
        //$request->session()->flash('message', 'Successfully updated role');
        return response()->json( ['status' => 'success'] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id, Request $request)
    {
        $userPosition = UserPosition::where('id', '=', $id)->first();

        $userPosition->delete();
        
        return response()->json( ['status' => 'success'] );
      
    }
}
