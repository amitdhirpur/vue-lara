<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              => 'required|min:1|max:256',
            'employee_id'       => 'required|min:1|max:6',
            'email'             => 'required|email|max:256',
            'contact_no'        => 'required|min:11|numeric',
            'position'          => 'required',
            'role'              => 'required',
            'birth_date'        => 'required'
        ];
    }
     public function messages()
    {
        return [
            'name.required' => 'Name is required!',
            'employee_id.max' => 'Employee Id is not more than six digit.',
            'employee_id.min' => 'Employee Id atleast one digit.',
            'email.required' => 'Email is required!',
            'contact_no.required' => 'Contact Number is required!',
            'position.required' => 'Position is required!',
            'role.required' => 'Role is required!',
            'birth_date.required' => 'Birth date is required!',
            'position.required' => 'Position is required!'
           
        ];
    }
}
