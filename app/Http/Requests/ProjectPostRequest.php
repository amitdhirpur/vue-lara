<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'       => 'required|min:1|max:256',
            'url'      => 'required',
            'client' => 'required',
            'portal'   => 'required',
            'profile'       => 'required'
        ];
    }
     public function messages()
    {
        return [
            'name.required' => 'Name is required!',
            'name.min' => 'Name is too small!',
            'url.required' => 'Url is required!',
            'client.required' => 'Client is required!',
            'portal.required' => 'Portal is required!',
            'profile.required' => 'Profile is required!'
        ];
    }
}
