<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Task extends Model
{
    use SoftDeletes;
    /**
	 * Get the project.
	 */
	public function projectModel()
	{
	    return $this->belongsTo('App\Models\Project', 'project');
	}

}
