(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[67],{

/***/ "../coreui/src/views/tasks/Addtask.vue":
/*!*********************************************!*\
  !*** ../coreui/src/views/tasks/Addtask.vue ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Addtask_vue_vue_type_template_id_407b8c6b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Addtask.vue?vue&type=template&id=407b8c6b& */ "../coreui/src/views/tasks/Addtask.vue?vue&type=template&id=407b8c6b&");
/* harmony import */ var _Addtask_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Addtask.vue?vue&type=script&lang=js& */ "../coreui/src/views/tasks/Addtask.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var vue_multiselect_dist_vue_multiselect_min_css_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue-multiselect/dist/vue-multiselect.min.css?vue&type=style&index=0&lang=css& */ "../coreui/node_modules/vue-multiselect/dist/vue-multiselect.min.css?vue&type=style&index=0&lang=css&");
/* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Addtask_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Addtask_vue_vue_type_template_id_407b8c6b___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Addtask_vue_vue_type_template_id_407b8c6b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/tasks/Addtask.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/tasks/Addtask.vue?vue&type=script&lang=js&":
/*!**********************************************************************!*\
  !*** ../coreui/src/views/tasks/Addtask.vue?vue&type=script&lang=js& ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Addtask_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./Addtask.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/tasks/Addtask.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Addtask_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/tasks/Addtask.vue?vue&type=template&id=407b8c6b&":
/*!****************************************************************************!*\
  !*** ../coreui/src/views/tasks/Addtask.vue?vue&type=template&id=407b8c6b& ***!
  \****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Addtask_vue_vue_type_template_id_407b8c6b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./Addtask.vue?vue&type=template&id=407b8c6b& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/tasks/Addtask.vue?vue&type=template&id=407b8c6b&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Addtask_vue_vue_type_template_id_407b8c6b___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Addtask_vue_vue_type_template_id_407b8c6b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/tasks/Addtask.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/tasks/Addtask.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "../coreui/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "../coreui/node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vuejs_datepicker__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuejs-datepicker */ "../coreui/node_modules/vuejs-datepicker/dist/vuejs-datepicker.esm.js");
/* harmony import */ var vue_multiselect__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vue-multiselect */ "../coreui/node_modules/vue-multiselect/dist/vue-multiselect.min.js");
/* harmony import */ var vue_multiselect__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(vue_multiselect__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! devextreme-vue/html-editor */ "../coreui/node_modules/devextreme-vue/html-editor.js");
/* harmony import */ var devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var vue2_dropzone__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vue2-dropzone */ "../coreui/node_modules/vue2-dropzone/dist/vue2Dropzone.js");
/* harmony import */ var vue2_dropzone__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(vue2_dropzone__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var vue2_dropzone_dist_vue2Dropzone_min_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! vue2-dropzone/dist/vue2Dropzone.min.css */ "../coreui/node_modules/vue2-dropzone/dist/vue2Dropzone.min.css");
/* harmony import */ var vue2_dropzone_dist_vue2Dropzone_min_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(vue2_dropzone_dist_vue2Dropzone_min_css__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! moment */ "../coreui/node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_7__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





var uuid = __webpack_require__(/*! uuid */ "../coreui/node_modules/uuid/dist/esm-browser/index.js");




/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Addtask',
  data: function data() {
    return {
      projects: [],
      customEditor: '',
      values: '',
      value: '',
      users: [],
      loader: '/img/loader.gif',
      alertClass: false,
      is_loading: false,
      is_shown: false,
      name: '',
      project: '',
      responsible: '',
      priority: '',
      description: '',
      deadline: new Date(),
      created_at: new Date(),
      message: '',
      dismissSecs: 7,
      dismissCountDown: 0,
      queryString: '',
      showDismissibleAlert: false,
      dropzoneOptions: {
        url: "https://httpbin.org/post",
        thumbnailWidth: 150,
        thumbnailHeight: 150,
        addRemoveLinks: true,
        dictDefaultMessage: "<p class='text-default dropbox-def-txt'><img src=\"/img/cloud-download.png\" /> Upload Images or Files Here.</p>"
      },
      images: []
    };
  },
  components: {
    Datepicker: vuejs_datepicker__WEBPACK_IMPORTED_MODULE_2__["default"],
    Multiselect: vue_multiselect__WEBPACK_IMPORTED_MODULE_3___default.a,
    DxHtmlEditor: devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_4__["DxHtmlEditor"],
    DxToolbar: devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_4__["DxToolbar"],
    DxItem: devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_4__["DxItem"],
    vueDropzone: vue2_dropzone__WEBPACK_IMPORTED_MODULE_5___default.a
  },
  methods: {
    goBack: function goBack() {
      this.$router.go(-1);
    },
    showAlert: function showAlert() {
      this.dismissCountDown = this.dismissSecs;
    },
    customFormatter: function customFormatter(date) {
      return moment__WEBPACK_IMPORTED_MODULE_7___default()(date).format('YYYY-MM-DD HH:mm:ss');
    },
    showDropbox: function showDropbox(e) {
      if (this.is_shown) {
        this.is_shown = false;
      } else {
        this.is_shown = true;
      }
    },
    addToeditor: function addToeditor(image) {
      var img = 'https://dev.team.teqtop.com/images/feeds/14/Screenshot%20from%202019-12-27%2018-09-27-1610368706737.png';
    },
    selectOption: function selectOption(e, type) {
      var div = document.querySelector('.editor-module'),
          clas = '.dx-' + type + '-format';
      document.querySelector(clas).click();
    },
    afterComplete: function afterComplete(upload) {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var imageName, that, file;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                imageName = uuid.v1();
                _this.isLoading = true;
                that = _this;

                try {
                  file = upload;
                  that.images.push(file);
                } catch (error) {
                  console.log(error);
                }

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    removeThisFile: function removeThisFile(file) {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var that, _id;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                that = _this2, _id = file.upload.uuid;

                _this2.images.forEach(function (item, i) {
                  if (item.upload.uuid === _id) {
                    that.images.splice(i, 1);
                  }
                });

              case 2:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    store: function store() {
      var observer = [],
          participants = [];

      for (var i = 0; i < this.value.length; i++) {
        observer.push(this.value[i].id);
      }

      for (var i = 0; i < this.values.length; i++) {
        participants.push(this.values[i].id);
      }

      var extras = {};
      extras.observer = this.value;
      extras.participants = this.values;
      var formdata = new FormData();
      formdata.append('name', this.name);

      if (this.queryString) {
        formdata.append('project', atob(this.queryString));
      } else {
        formdata.append('project', this.project);
      }

      formdata.append('responsible', this.responsible);
      formdata.append('priority', this.priority);
      formdata.append('observer', JSON.stringify(observer));
      formdata.append('participants', JSON.stringify(participants));
      formdata.append('extras', JSON.stringify(extras));
      formdata.append('description', this.description);
      formdata.append('deadline', this.customFormatter(this.deadline));
      formdata.append('created_at', this.customFormatter(this.created_at));

      for (var i = 0; i < this.images.length; i++) {
        var file = this.images[i];
        formdata.append('files[' + i + ']', file);
      }

      var that = this;
      this.is_loading = true; // api to save task.

      axios__WEBPACK_IMPORTED_MODULE_1___default.a.post('/api/tasks/add?token=' + localStorage.getItem("api_token"), formdata).then(function (response) {
        that.is_loading = false; // that.message = response.data.message;
        // that.showAlert();
        // that.$store.dispatch('topFunction');

        that.$router.push({
          path: '/tasks'
        });
      });
    }
  },
  mounted: function mounted() {
    var self = this;
    self.queryString = self.$route.query.project;
    axios__WEBPACK_IMPORTED_MODULE_1___default.a.get('/api/projects/list/?token=' + localStorage.getItem("api_token")).then(function (response) {
      self.projects = response.data.projects;
      self.users = response.data.users;
    });
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/tasks/Addtask.vue?vue&type=template&id=407b8c6b&":
/*!**********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/tasks/Addtask.vue?vue&type=template&id=407b8c6b& ***!
  \**********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CRow",
    [
      _c(
        "CCol",
        { attrs: { col: "12", lg: "12" } },
        [
          _c(
            "CCard",
            { attrs: { "no-header": "" } },
            [
              _c(
                "CCardBody",
                [
                  _c(
                    "h3",
                    [
                      _vm._v("Add Task "),
                      _c(
                        "CButton",
                        {
                          staticClass: "float-right",
                          attrs: { color: "primary" },
                          on: { click: _vm.goBack }
                        },
                        [_vm._v("Back")]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "CAlert",
                    {
                      attrs: {
                        show: _vm.dismissCountDown,
                        color: _vm.alertClass ? "success" : "danger",
                        fade: ""
                      },
                      on: {
                        "update:show": function($event) {
                          _vm.dismissCountDown = $event
                        }
                      }
                    },
                    [
                      _vm._v(
                        "\n\t\t            \t(" +
                          _vm._s(_vm.dismissCountDown) +
                          ") " +
                          _vm._s(_vm.message) +
                          "\n\t\t          \t"
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "CForm",
                    {
                      attrs: { method: "POST" },
                      on: {
                        submit: function($event) {
                          $event.preventDefault()
                          return _vm.store($event)
                        }
                      }
                    },
                    [
                      _c("div", { staticClass: "row" }, [
                        _c(
                          "div",
                          { staticClass: "col" },
                          [
                            _c("CInput", {
                              attrs: {
                                label: "Name *",
                                type: "text",
                                placeholder: "Name",
                                required: ""
                              },
                              model: {
                                value: _vm.name,
                                callback: function($$v) {
                                  _vm.name = $$v
                                },
                                expression: "name"
                              }
                            })
                          ],
                          1
                        ),
                        _vm._v(" "),
                        !_vm.queryString
                          ? _c("div", { staticClass: "col" }, [
                              _c("div", { staticClass: "form-group" }, [
                                _c("label", [_vm._v("Project *")]),
                                _vm._v(" "),
                                _c(
                                  "select",
                                  {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.project,
                                        expression: "project"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: { required: "" },
                                    on: {
                                      change: function($event) {
                                        var $$selectedVal = Array.prototype.filter
                                          .call($event.target.options, function(
                                            o
                                          ) {
                                            return o.selected
                                          })
                                          .map(function(o) {
                                            var val =
                                              "_value" in o ? o._value : o.value
                                            return val
                                          })
                                        _vm.project = $event.target.multiple
                                          ? $$selectedVal
                                          : $$selectedVal[0]
                                      }
                                    }
                                  },
                                  [
                                    _c("option", { attrs: { value: "" } }, [
                                      _vm._v("--Select Project--")
                                    ]),
                                    _vm._v(" "),
                                    _vm._l(_vm.projects, function(project) {
                                      return _c(
                                        "option",
                                        { domProps: { value: project.id } },
                                        [_vm._v(_vm._s(project.name))]
                                      )
                                    })
                                  ],
                                  2
                                )
                              ])
                            ])
                          : _vm._e()
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "row" }, [
                        _c("div", { staticClass: "col" }, [
                          _c("div", { staticClass: "form-group" }, [
                            _c("label", [_vm._v("Responsible Person *")]),
                            _vm._v(" "),
                            _c(
                              "select",
                              {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.responsible,
                                    expression: "responsible"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: { required: "" },
                                on: {
                                  change: function($event) {
                                    var $$selectedVal = Array.prototype.filter
                                      .call($event.target.options, function(o) {
                                        return o.selected
                                      })
                                      .map(function(o) {
                                        var val =
                                          "_value" in o ? o._value : o.value
                                        return val
                                      })
                                    _vm.responsible = $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  }
                                }
                              },
                              [
                                _c("option", { attrs: { value: "" } }, [
                                  _vm._v("--Select Person--")
                                ]),
                                _vm._v(" "),
                                _vm._l(_vm.users, function(user) {
                                  return _c(
                                    "option",
                                    { domProps: { value: user.id } },
                                    [_vm._v(_vm._s(user.name))]
                                  )
                                })
                              ],
                              2
                            )
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "col" }, [
                          _c("div", { staticClass: "form-group" }, [
                            _c("label", [_vm._v("Priority")]),
                            _vm._v(" "),
                            _c(
                              "select",
                              {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.priority,
                                    expression: "priority"
                                  }
                                ],
                                staticClass: "form-control",
                                on: {
                                  change: function($event) {
                                    var $$selectedVal = Array.prototype.filter
                                      .call($event.target.options, function(o) {
                                        return o.selected
                                      })
                                      .map(function(o) {
                                        var val =
                                          "_value" in o ? o._value : o.value
                                        return val
                                      })
                                    _vm.priority = $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  }
                                }
                              },
                              [
                                _c("option", { attrs: { value: "" } }, [
                                  _vm._v("--Select Priority--")
                                ]),
                                _vm._v(" "),
                                _c("option", { attrs: { value: "0" } }, [
                                  _vm._v("Low")
                                ]),
                                _vm._v(" "),
                                _c("option", { attrs: { value: "1" } }, [
                                  _vm._v("Medium")
                                ]),
                                _vm._v(" "),
                                _c("option", { attrs: { value: "2" } }, [
                                  _vm._v("High")
                                ])
                              ]
                            )
                          ])
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "row" }, [
                        _c("div", { staticClass: "col" }, [
                          _c(
                            "div",
                            { staticClass: "form-group" },
                            [
                              _c("label", [_vm._v("Participants")]),
                              _vm._v(" "),
                              _c("multiselect", {
                                attrs: {
                                  "track-by": "id",
                                  label: "name",
                                  searchable: "",
                                  multiple: "",
                                  options: _vm.users
                                },
                                model: {
                                  value: _vm.values,
                                  callback: function($$v) {
                                    _vm.values = $$v
                                  },
                                  expression: "values"
                                }
                              })
                            ],
                            1
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "col" }, [
                          _c(
                            "div",
                            { staticClass: "form-group" },
                            [
                              _c("label", [_vm._v("Observers")]),
                              _vm._v(" "),
                              _c("multiselect", {
                                attrs: {
                                  "track-by": "id",
                                  label: "name",
                                  searchable: "",
                                  multiple: "",
                                  options: _vm.users
                                },
                                model: {
                                  value: _vm.value,
                                  callback: function($$v) {
                                    _vm.value = $$v
                                  },
                                  expression: "value"
                                }
                              })
                            ],
                            1
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "row" }, [
                        _c("div", { staticClass: "col" }, [
                          _c(
                            "div",
                            { staticClass: "form-group" },
                            [
                              _c("label", [_vm._v("Created Date")]),
                              _vm._v(" "),
                              _c("datepicker", {
                                attrs: {
                                  format: _vm.customFormatter,
                                  placeholder: "Created Date",
                                  "input-class": "form-control"
                                },
                                model: {
                                  value: _vm.created_at,
                                  callback: function($$v) {
                                    _vm.created_at = $$v
                                  },
                                  expression: "created_at"
                                }
                              })
                            ],
                            1
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "col" }, [
                          _c(
                            "div",
                            { staticClass: "form-group" },
                            [
                              _c("label", [_vm._v("Deadline")]),
                              _vm._v(" "),
                              _c("datepicker", {
                                attrs: {
                                  format: _vm.customFormatter,
                                  placeholder: "Deadline",
                                  "input-class": "form-control"
                                },
                                model: {
                                  value: _vm.deadline,
                                  callback: function($$v) {
                                    _vm.deadline = $$v
                                  },
                                  expression: "deadline"
                                }
                              })
                            ],
                            1
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "editor-module" },
                        [
                          _c(
                            "DxHtmlEditor",
                            {
                              ref: "textEditor",
                              attrs: { height: "300px" },
                              model: {
                                value: _vm.description,
                                callback: function($$v) {
                                  _vm.description = $$v
                                },
                                expression: "description"
                              }
                            },
                            [
                              _c(
                                "DxToolbar",
                                [
                                  _c("DxItem", {
                                    attrs: { "format-name": "bold" }
                                  }),
                                  _vm._v(" "),
                                  _c("DxItem", {
                                    attrs: { "format-name": "link" }
                                  }),
                                  _vm._v(" "),
                                  _c("DxItem", {
                                    attrs: { "format-name": "blockquote" }
                                  })
                                ],
                                1
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("div", { staticClass: "editor-icons" }, [
                            _c(
                              "a",
                              {
                                staticClass: "attachment-img-ico",
                                on: {
                                  click: function($event) {
                                    return _vm.showDropbox($event)
                                  }
                                }
                              },
                              [
                                _c("img", {
                                  attrs: { src: "/img/attachment.png", alt: "" }
                                })
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "a",
                              {
                                staticClass: "bold-img-ico",
                                on: {
                                  click: function($event) {
                                    return _vm.selectOption($event, "bold")
                                  }
                                }
                              },
                              [
                                _c("img", {
                                  attrs: { src: "/img/bold.png", alt: "" }
                                })
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "a",
                              {
                                on: {
                                  click: function($event) {
                                    return _vm.selectOption($event, "link")
                                  }
                                }
                              },
                              [_c("CIcon", { attrs: { name: "cil-link" } })],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "a",
                              {
                                staticClass: "quoted-img-ico",
                                on: {
                                  click: function($event) {
                                    return _vm.selectOption(
                                      $event,
                                      "blockquote"
                                    )
                                  }
                                }
                              },
                              [
                                _c("img", {
                                  attrs: { src: "/img/quote.png", alt: "" }
                                })
                              ]
                            )
                          ]),
                          _vm._v(" "),
                          _vm.images.length > 0
                            ? _c(
                                "div",
                                { staticClass: "files-names" },
                                _vm._l(_vm.images, function(image, i) {
                                  return _c("p", [
                                    _vm._v(
                                      _vm._s(image.name) +
                                        " \n      \t\t\t\t\t\t\t\t"
                                    ),
                                    _c(
                                      "a",
                                      {
                                        staticClass: "btn",
                                        on: {
                                          click: function($event) {
                                            return _vm.addToeditor(image)
                                          }
                                        }
                                      },
                                      [_vm._v("Insert File")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "a",
                                      {
                                        staticClass: "float-right",
                                        on: {
                                          click: function($event) {
                                            return _vm.removeIndex(image)
                                          }
                                        }
                                      },
                                      [
                                        _c("CIcon", {
                                          attrs: { name: "cil-x" }
                                        })
                                      ],
                                      1
                                    )
                                  ])
                                }),
                                0
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.is_shown
                            ? _c("vue-dropzone", {
                                ref: "imgDropZone",
                                attrs: {
                                  id: "customdropzone",
                                  options: _vm.dropzoneOptions
                                },
                                on: {
                                  "vdropzone-complete": _vm.afterComplete,
                                  "vdropzone-removed-file": _vm.removeThisFile
                                }
                              })
                            : _vm._e()
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "CButton",
                        {
                          staticClass: "mt-3",
                          attrs: { color: "primary", type: "submit" }
                        },
                        [_vm._v("Save")]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _vm.is_loading
        ? _c("div", { staticClass: "loader-image" }, [
            _c("img", { attrs: { src: _vm.loader } })
          ])
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);