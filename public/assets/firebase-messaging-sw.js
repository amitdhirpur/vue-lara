importScripts('https://www.gstatic.com/firebasejs/8.2.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.2.1/firebase-messaging.js');
var firebaseConfig = {
      apiKey: "AIzaSyCl58SJ3W0VdPAjjOin0gfnthcmxqW4Wyg",
      authDomain: "teqtopportal.firebaseapp.com",
      databaseURL: "https://teqtopportal-default-rtdb.firebaseio.com",
      projectId: "teqtopportal",
      storageBucket: "teqtopportal.appspot.com",
      messagingSenderId: "192919426920",
      appId: "1:192919426920:web:8e8c4b9cde2502912ae8dd",
      measurementId: "G-6PENXZGLPM"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
    console.log("here");
    // Customize notification here
    const notificationTitle = payload.data.title;
    const notificationOptions = {
        body: payload.data.body,
        icon: payload.data.icon,
        image:  payload.data.image,
        data: {
          url: payload.data.click_action
        } 
    };
  

    return self.registration.showNotification(
        notificationTitle,
        notificationOptions,
    );
});

self.addEventListener('push', function(event) {
  console.log('Received a push message', event);

  var title = 'Yay a message.';
  var body = 'We have received a push message.';
  var icon = '/images/icon-192x192.png';
  var tag = 'simple-push-demo-notification-tag';

  event.waitUntil(
    self.registration.showNotification(title, {
      body: body,
      icon: icon,
      tag: tag
    })
  );
});


