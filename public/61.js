(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[61],{

/***/ "../coreui/node_modules/devextreme-vue/text-area.js":
/*!**********************************************************!*\
  !*** ../coreui/node_modules/devextreme-vue/text-area.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/*!
 * devextreme-vue
 * Version: 20.2.4
 * Build date: Tue Dec 01 2020
 *
 * Copyright (c) 2012 - 2020 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */


Object.defineProperty(exports, "__esModule", { value: true });
exports.DxTextArea = void 0;
var text_area_1 = __webpack_require__(/*! devextreme/ui/text_area */ "../coreui/node_modules/devextreme/ui/text_area.js");
var index_1 = __webpack_require__(/*! ./core/index */ "../coreui/node_modules/devextreme-vue/core/index.js");
var DxTextArea = index_1.createComponent({
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        autoResizeEnabled: Boolean,
        disabled: Boolean,
        elementAttr: Object,
        focusStateEnabled: Boolean,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        inputAttr: Object,
        isValid: Boolean,
        maxHeight: [Number, String],
        maxLength: [Number, String],
        minHeight: [Number, String],
        name: String,
        onChange: Function,
        onContentReady: Function,
        onCopy: Function,
        onCut: Function,
        onDisposing: Function,
        onEnterKey: Function,
        onFocusIn: Function,
        onFocusOut: Function,
        onInitialized: Function,
        onInput: Function,
        onKeyDown: Function,
        onKeyUp: Function,
        onOptionChanged: Function,
        onPaste: Function,
        onValueChanged: Function,
        placeholder: String,
        readOnly: Boolean,
        rtlEnabled: Boolean,
        spellcheck: Boolean,
        stylingMode: String,
        tabIndex: Number,
        text: String,
        validationError: Object,
        validationErrors: Array,
        validationMessageMode: String,
        validationStatus: String,
        value: String,
        valueChangeEvent: String,
        visible: Boolean,
        width: [Function, Number, String]
    },
    emits: {
        "update:isActive": null,
        "update:hoveredElement": null,
        "update:accessKey": null,
        "update:activeStateEnabled": null,
        "update:autoResizeEnabled": null,
        "update:disabled": null,
        "update:elementAttr": null,
        "update:focusStateEnabled": null,
        "update:height": null,
        "update:hint": null,
        "update:hoverStateEnabled": null,
        "update:inputAttr": null,
        "update:isValid": null,
        "update:maxHeight": null,
        "update:maxLength": null,
        "update:minHeight": null,
        "update:name": null,
        "update:onChange": null,
        "update:onContentReady": null,
        "update:onCopy": null,
        "update:onCut": null,
        "update:onDisposing": null,
        "update:onEnterKey": null,
        "update:onFocusIn": null,
        "update:onFocusOut": null,
        "update:onInitialized": null,
        "update:onInput": null,
        "update:onKeyDown": null,
        "update:onKeyUp": null,
        "update:onOptionChanged": null,
        "update:onPaste": null,
        "update:onValueChanged": null,
        "update:placeholder": null,
        "update:readOnly": null,
        "update:rtlEnabled": null,
        "update:spellcheck": null,
        "update:stylingMode": null,
        "update:tabIndex": null,
        "update:text": null,
        "update:validationError": null,
        "update:validationErrors": null,
        "update:validationMessageMode": null,
        "update:validationStatus": null,
        "update:value": null,
        "update:valueChangeEvent": null,
        "update:visible": null,
        "update:width": null,
    },
    model: { prop: "value", event: "update:value" },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = text_area_1.default;
    }
});
exports.DxTextArea = DxTextArea;
exports.default = DxTextArea;


/***/ }),

/***/ "../coreui/node_modules/devextreme/ui/text_area.js":
/*!*********************************************************!*\
  !*** ../coreui/node_modules/devextreme/ui/text_area.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * DevExtreme (ui/text_area.js)
 * Version: 20.2.4
 * Build date: Tue Dec 01 2020
 *
 * Copyright (c) 2012 - 2020 Developer Express Inc. ALL RIGHTS RESERVED
 * Read about DevExtreme licensing here: https://js.devexpress.com/Licensing/
 */

exports.default = void 0;
var _renderer = _interopRequireDefault(__webpack_require__(/*! ../core/renderer */ "../coreui/node_modules/devextreme/core/renderer.js"));
var _events_engine = _interopRequireDefault(__webpack_require__(/*! ../events/core/events_engine */ "../coreui/node_modules/devextreme/events/core/events_engine.js"));
var _common = __webpack_require__(/*! ../core/utils/common */ "../coreui/node_modules/devextreme/core/utils/common.js");
var _window = __webpack_require__(/*! ../core/utils/window */ "../coreui/node_modules/devextreme/core/utils/window.js");
var _component_registrator = _interopRequireDefault(__webpack_require__(/*! ../core/component_registrator */ "../coreui/node_modules/devextreme/core/component_registrator.js"));
var _extend = __webpack_require__(/*! ../core/utils/extend */ "../coreui/node_modules/devextreme/core/utils/extend.js");
var _type = __webpack_require__(/*! ../core/utils/type */ "../coreui/node_modules/devextreme/core/utils/type.js");
var _index = __webpack_require__(/*! ../events/utils/index */ "../coreui/node_modules/devextreme/events/utils/index.js");
var _pointer = _interopRequireDefault(__webpack_require__(/*! ../events/pointer */ "../coreui/node_modules/devextreme/events/pointer.js"));
var _uiEventsEmitterGesture = _interopRequireDefault(__webpack_require__(/*! ../ui/scroll_view/ui.events.emitter.gesture.scroll */ "../coreui/node_modules/devextreme/ui/scroll_view/ui.events.emitter.gesture.scroll.js"));
var _size = __webpack_require__(/*! ../core/utils/size */ "../coreui/node_modules/devextreme/core/utils/size.js");
var _utils = __webpack_require__(/*! ./text_box/utils.scroll */ "../coreui/node_modules/devextreme/ui/text_box/utils.scroll.js");
var _text_box = _interopRequireDefault(__webpack_require__(/*! ./text_box */ "../coreui/node_modules/devextreme/ui/text_box.js"));

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        "default": obj
    }
}
var TEXTAREA_CLASS = "dx-textarea";
var TEXTEDITOR_INPUT_CLASS = "dx-texteditor-input";
var TEXTEDITOR_INPUT_CLASS_AUTO_RESIZE = "dx-texteditor-input-auto-resize";
var TextArea = _text_box.default.inherit({
    _getDefaultOptions: function() {
        return (0, _extend.extend)(this.callBase(), {
            spellcheck: true,
            minHeight: void 0,
            maxHeight: void 0,
            autoResizeEnabled: false
        })
    },
    _initMarkup: function() {
        this.$element().addClass(TEXTAREA_CLASS);
        this.callBase();
        this.setAria("multiline", "true")
    },
    _renderContentImpl: function() {
        this._updateInputHeight();
        this.callBase()
    },
    _renderInput: function() {
        this.callBase();
        this._renderScrollHandler()
    },
    _createInput: function() {
        var $input = (0, _renderer.default)("<textarea>");
        this._applyInputAttributes($input, this.option("inputAttr"));
        this._updateInputAutoResizeAppearance($input);
        return $input
    },
    _applyInputAttributes: function($input, customAttributes) {
        $input.attr(customAttributes).addClass(TEXTEDITOR_INPUT_CLASS)
    },
    _renderScrollHandler: function() {
        this._eventY = 0;
        var $input = this._input();
        var initScrollData = (0, _utils.prepareScrollData)($input, true);
        _events_engine.default.on($input, (0, _index.addNamespace)(_uiEventsEmitterGesture.default.init, this.NAME), initScrollData, _common.noop);
        _events_engine.default.on($input, (0, _index.addNamespace)(_pointer.default.down, this.NAME), this._pointerDownHandler.bind(this));
        _events_engine.default.on($input, (0, _index.addNamespace)(_pointer.default.move, this.NAME), this._pointerMoveHandler.bind(this))
    },
    _pointerDownHandler: function(e) {
        this._eventY = (0, _index.eventData)(e).y
    },
    _pointerMoveHandler: function(e) {
        var currentEventY = (0, _index.eventData)(e).y;
        var delta = this._eventY - currentEventY;
        if ((0, _utils.allowScroll)(this._input(), delta)) {
            e.isScrollingEvent = true;
            e.stopPropagation()
        }
        this._eventY = currentEventY
    },
    _renderDimensions: function() {
        var $element = this.$element();
        var element = $element.get(0);
        var width = this._getOptionValue("width", element);
        var height = this._getOptionValue("height", element);
        var minHeight = this.option("minHeight");
        var maxHeight = this.option("maxHeight");
        $element.css({
            minHeight: void 0 !== minHeight ? minHeight : "",
            maxHeight: void 0 !== maxHeight ? maxHeight : "",
            width: width,
            height: height
        })
    },
    _resetDimensions: function() {
        this.$element().css({
            height: "",
            minHeight: "",
            maxHeight: ""
        })
    },
    _renderEvents: function() {
        if (this.option("autoResizeEnabled")) {
            _events_engine.default.on(this._input(), (0, _index.addNamespace)("input paste", this.NAME), this._updateInputHeight.bind(this))
        }
        this.callBase()
    },
    _refreshEvents: function() {
        _events_engine.default.off(this._input(), (0, _index.addNamespace)("input paste", this.NAME));
        this.callBase()
    },
    _getHeightDifference: function($input) {
        return (0, _size.getVerticalOffsets)(this._$element.get(0), false) + (0, _size.getVerticalOffsets)(this._$textEditorContainer.get(0), false) + (0, _size.getVerticalOffsets)(this._$textEditorInputContainer.get(0), false) + (0, _size.getElementBoxParams)("height", (0, _window.getWindow)().getComputedStyle($input.get(0))).margin
    },
    _updateInputHeight: function() {
        var $input = this._input();
        var autoHeightResizing = void 0 === this.option("height") && this.option("autoResizeEnabled");
        if (!autoHeightResizing) {
            $input.css("height", "");
            return
        } else {
            this._resetDimensions();
            this._$element.css("height", this._$element.outerHeight())
        }
        $input.css("height", 0);
        var heightDifference = this._getHeightDifference($input);
        this._renderDimensions();
        var minHeight = this._getBoundaryHeight("minHeight");
        var maxHeight = this._getBoundaryHeight("maxHeight");
        var inputHeight = $input[0].scrollHeight;
        if (void 0 !== minHeight) {
            inputHeight = Math.max(inputHeight, minHeight - heightDifference)
        }
        if (void 0 !== maxHeight) {
            var adjustedMaxHeight = maxHeight - heightDifference;
            var needScroll = inputHeight > adjustedMaxHeight;
            inputHeight = Math.min(inputHeight, adjustedMaxHeight);
            this._updateInputAutoResizeAppearance($input, !needScroll)
        }
        $input.css("height", inputHeight);
        if (autoHeightResizing) {
            this._$element.css("height", "auto")
        }
    },
    _getBoundaryHeight: function(optionName) {
        var boundaryValue = this.option(optionName);
        if ((0, _type.isDefined)(boundaryValue)) {
            return "number" === typeof boundaryValue ? boundaryValue : (0, _size.parseHeight)(boundaryValue, this._$textEditorContainer.get(0))
        }
    },
    _renderInputType: _common.noop,
    _visibilityChanged: function(visible) {
        if (visible) {
            this._updateInputHeight()
        }
    },
    _updateInputAutoResizeAppearance: function($input, isAutoResizeEnabled) {
        if ($input) {
            var autoResizeEnabled = (0, _common.ensureDefined)(isAutoResizeEnabled, this.option("autoResizeEnabled"));
            $input.toggleClass(TEXTEDITOR_INPUT_CLASS_AUTO_RESIZE, autoResizeEnabled)
        }
    },
    _optionChanged: function(args) {
        switch (args.name) {
            case "autoResizeEnabled":
                this._updateInputAutoResizeAppearance(this._input(), args.value);
                this._refreshEvents();
                this._updateInputHeight();
                break;
            case "value":
            case "height":
                this.callBase(args);
                this._updateInputHeight();
                break;
            case "minHeight":
            case "maxHeight":
                this._renderDimensions();
                this._updateInputHeight();
                break;
            case "visible":
                this.callBase(args);
                args.value && this._updateInputHeight();
                break;
            default:
                this.callBase(args)
        }
    }
});
(0, _component_registrator.default)("dxTextArea", TextArea);
var _default = TextArea;
exports.default = _default;
module.exports = exports.default;
module.exports.default = module.exports;


/***/ })

}]);