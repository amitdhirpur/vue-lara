(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[29],{

/***/ "../coreui/src/views/employees/Employee.vue":
/*!**************************************************!*\
  !*** ../coreui/src/views/employees/Employee.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Employee_vue_vue_type_template_id_468a85bc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Employee.vue?vue&type=template&id=468a85bc& */ "../coreui/src/views/employees/Employee.vue?vue&type=template&id=468a85bc&");
/* harmony import */ var _Employee_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Employee.vue?vue&type=script&lang=js& */ "../coreui/src/views/employees/Employee.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Employee_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Employee_vue_vue_type_template_id_468a85bc___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Employee_vue_vue_type_template_id_468a85bc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/employees/Employee.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/employees/Employee.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ../coreui/src/views/employees/Employee.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Employee_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./Employee.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/Employee.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Employee_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/employees/Employee.vue?vue&type=template&id=468a85bc&":
/*!*********************************************************************************!*\
  !*** ../coreui/src/views/employees/Employee.vue?vue&type=template&id=468a85bc& ***!
  \*********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Employee_vue_vue_type_template_id_468a85bc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./Employee.vue?vue&type=template&id=468a85bc& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/Employee.vue?vue&type=template&id=468a85bc&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Employee_vue_vue_type_template_id_468a85bc___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Employee_vue_vue_type_template_id_468a85bc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/Employee.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/employees/Employee.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "../coreui/node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "../coreui/node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'User',
  data: function data() {
    return {
      name: '',
      created_on: '',
      employee_id: '',
      email: '',
      role: '',
      date_format: '',
      address: '',
      department: '',
      profile: '',
      contact_no: '',
      alternate_no: '',
      position: '',
      birth_date: '',
      joining_date: '',
      additional_info: '',
      appraisel_date: '',
      current_address: '',
      permanent_address: '',
      status: '',
      loading: false,
      avtar: '/img/user-avtar.jpg',
      loader: '/img/loader.gif'
    };
  },
  methods: {
    goBack: function goBack() {
      this.$router.go(-1);
    },
    editUser: function editUser() {
      this.$router.push({
        path: "/employees/".concat(this.$route.params.id, "/edit")
      });
    },
    dateFormat: function dateFormat(elem) {
      var value = "";

      if (elem) {
        value = moment__WEBPACK_IMPORTED_MODULE_1___default()(elem).format(this.date_format);
      }

      return value;
    }
  },
  mounted: function mounted() {
    var self = this;
    this.loading = true;
    axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('/api/users/' + self.$route.params.id + '?token=' + localStorage.getItem("api_token")).then(function (response) {
      self.loading = false;
      self.name = response.data.name;
      self.created_on = response.data.created_at;
      self.employee_id = response.data.employee_id;
      self.email = response.data.email;
      self.role = response.data.menuroles;
      self.department = response.data.department;
      self.contact_no = response.data.contact_no;
      self.alternate_no = response.data.alternate_no;
      self.position = response.data.user_position;
      self.birth_date = response.data.birth_date;
      self.joining_date = response.data.joining_date;
      self.additional_info = response.data.additional_info;
      self.appraisel_date = response.data.appraisel_date;
      self.current_address = response.data.current_address;
      self.permanent_address = response.data.permanent_address;
      self.profile = response.data.profile;
      self.status = response.data.is_active;
    })["catch"](function (error) {
      self.loading = false;
      self.$router.push({
        path: '/login'
      });
    });
    this.date_format = localStorage.getItem('date_format');
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/Employee.vue?vue&type=template&id=468a85bc&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/employees/Employee.vue?vue&type=template&id=468a85bc& ***!
  \***************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CRow",
    [
      _c(
        "CCol",
        { attrs: { col: "12", lg: "12" } },
        [
          _c(
            "CCardHeader",
            {
              staticClass:
                "mb-1 d-flex justify-content-between align-items-center"
            },
            [
              _c("h4", { staticClass: "mb-0" }, [_vm._v(_vm._s(_vm.name))]),
              _vm._v(" "),
              _c(
                "div",
                [
                  _c(
                    "CButton",
                    {
                      attrs: { color: "primary" },
                      on: { click: _vm.editUser }
                    },
                    [
                      _c("CIcon", { attrs: { name: "cil-pencil" } }),
                      _vm._v(" Edit")
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "CButton",
                    { attrs: { color: "primary" }, on: { click: _vm.goBack } },
                    [_vm._v("Back")]
                  )
                ],
                1
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "CRow",
            { staticClass: "user-view-sec" },
            [
              _c(
                "CCol",
                { attrs: { col: "5" } },
                [
                  _c(
                    "CCard",
                    [
                      _c("CCardBody", [
                        _c("div", { staticClass: "user-profile-pic" }, [
                          _vm.profile
                            ? _c("img", { attrs: { src: _vm.profile } })
                            : _c("img", { attrs: { src: _vm.avtar } })
                        ])
                      ])
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "CCol",
                { attrs: { col: "7" } },
                [
                  _c(
                    "CCard",
                    [
                      _c(
                        "CCardBody",
                        [
                          _c("CCardHeader", [_vm._v("Contact information")]),
                          _vm._v(" "),
                          _c("div", { staticClass: "user-info-section" }, [
                            _vm.employee_id
                              ? _c("div", { staticClass: "inner-sec" }, [
                                  _c("span", [_vm._v("Employee Id:")]),
                                  _vm._v(" "),
                                  _c("strong", [
                                    _vm._v(_vm._s(_vm.employee_id))
                                  ])
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.email
                              ? _c("div", { staticClass: "inner-sec" }, [
                                  _c("span", [_vm._v("Email:")]),
                                  _vm._v(" "),
                                  _c("strong", [_vm._v(_vm._s(_vm.email))])
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.birth_date
                              ? _c("div", { staticClass: "inner-sec" }, [
                                  _c("span", [_vm._v("Date Of Birth:")]),
                                  _vm._v(" "),
                                  _c("strong", [
                                    _vm._v(
                                      _vm._s(_vm.dateFormat(this.birth_date))
                                    )
                                  ])
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.contact_no
                              ? _c("div", { staticClass: "inner-sec" }, [
                                  _c("span", [_vm._v("Contact No:")]),
                                  _vm._v(" "),
                                  _c("strong", [_vm._v(_vm._s(_vm.contact_no))])
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.address
                              ? _c("div", { staticClass: "inner-sec" }, [
                                  _c("span", [_vm._v("Address:")]),
                                  _vm._v(" "),
                                  _c("strong", [_vm._v(_vm._s(_vm.address))])
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.alternate_no
                              ? _c("div", { staticClass: "inner-sec" }, [
                                  _c("span", [_vm._v("Alternate No:")]),
                                  _vm._v(" "),
                                  _c("strong", [
                                    _vm._v(_vm._s(_vm.alternate_no))
                                  ])
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.department
                              ? _c("div", { staticClass: "inner-sec" }, [
                                  _c("span", [_vm._v("Department:")]),
                                  _vm._v(" "),
                                  _c("strong", [_vm._v(_vm._s(_vm.department))])
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.role
                              ? _c("div", { staticClass: "inner-sec" }, [
                                  _c("span", [_vm._v("Role:")]),
                                  _vm._v(" "),
                                  _c("strong", [_vm._v(_vm._s(_vm.role))])
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.position
                              ? _c("div", { staticClass: "inner-sec" }, [
                                  _c("span", [_vm._v("Position:")]),
                                  _vm._v(" "),
                                  _c("strong", [_vm._v(_vm._s(_vm.position))])
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.created_on
                              ? _c("div", { staticClass: "inner-sec" }, [
                                  _c("span", [_vm._v("Registered On:")]),
                                  _vm._v(" "),
                                  _c("strong", [
                                    _vm._v(
                                      _vm._s(_vm.dateFormat(this.created_on))
                                    )
                                  ])
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.appraisel_date
                              ? _c("div", { staticClass: "inner-sec" }, [
                                  _c("span", [_vm._v("Appraisal Dates:")]),
                                  _vm._v(" "),
                                  _c("strong", [
                                    _vm._v(
                                      _vm._s(
                                        _vm.dateFormat(this.appraisel_date)
                                      )
                                    )
                                  ])
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.joining_date
                              ? _c("div", { staticClass: "inner-sec" }, [
                                  _c("span", [_vm._v("Joining Date:")]),
                                  _vm._v(" "),
                                  _c("strong", [
                                    _vm._v(
                                      _vm._s(_vm.dateFormat(this.joining_date))
                                    )
                                  ])
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.current_address
                              ? _c("div", { staticClass: "inner-sec" }, [
                                  _c("span", [_vm._v("Current Address:")]),
                                  _vm._v(" "),
                                  _c("strong", [
                                    _vm._v(_vm._s(_vm.current_address))
                                  ])
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.permanent_address
                              ? _c("div", { staticClass: "inner-sec" }, [
                                  _c("span", [_vm._v("Permanent Address:")]),
                                  _vm._v(" "),
                                  _c("strong", [
                                    _vm._v(_vm._s(_vm.permanent_address))
                                  ])
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.employee_id
                              ? _c(
                                  "div",
                                  { staticClass: "inner-sec" },
                                  [
                                    _c("span", [_vm._v("Status:")]),
                                    _vm._v(" "),
                                    _vm.status == "1"
                                      ? _c(
                                          "CBadge",
                                          { attrs: { color: "success" } },
                                          [_vm._v("Active")]
                                        )
                                      : _c(
                                          "CBadge",
                                          { attrs: { color: "danger" } },
                                          [_vm._v("Deactive")]
                                        )
                                  ],
                                  1
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.additional_info
                              ? _c("div", { staticClass: "inner-sec" }, [
                                  _c("span", [_vm._v("Additional Info:")]),
                                  _vm._v(" "),
                                  _c("span", {
                                    domProps: {
                                      innerHTML: _vm._s(_vm.additional_info)
                                    }
                                  })
                                ])
                              : _vm._e()
                          ])
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _vm.loading
        ? _c("div", { staticClass: "loader-image" }, [
            _c("img", { attrs: { src: _vm.loader } })
          ])
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);