(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[52],{

/***/ "../coreui/src/views/reports/Reports.vue":
/*!***********************************************!*\
  !*** ../coreui/src/views/reports/Reports.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Reports_vue_vue_type_template_id_5d5a2596___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Reports.vue?vue&type=template&id=5d5a2596& */ "../coreui/src/views/reports/Reports.vue?vue&type=template&id=5d5a2596&");
/* harmony import */ var _Reports_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Reports.vue?vue&type=script&lang=js& */ "../coreui/src/views/reports/Reports.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Reports_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Reports_vue_vue_type_template_id_5d5a2596___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Reports_vue_vue_type_template_id_5d5a2596___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/reports/Reports.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/reports/Reports.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ../coreui/src/views/reports/Reports.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Reports_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./Reports.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/reports/Reports.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Reports_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/reports/Reports.vue?vue&type=template&id=5d5a2596&":
/*!******************************************************************************!*\
  !*** ../coreui/src/views/reports/Reports.vue?vue&type=template&id=5d5a2596& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Reports_vue_vue_type_template_id_5d5a2596___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./Reports.vue?vue&type=template&id=5d5a2596& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/reports/Reports.vue?vue&type=template&id=5d5a2596&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Reports_vue_vue_type_template_id_5d5a2596___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Reports_vue_vue_type_template_id_5d5a2596___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/reports/Reports.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/reports/Reports.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "../coreui/node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "../coreui/node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Reports',
  data: function data() {
    return {
      items: [],
      users: [],
      fields: ['name', 'report', 'report_date', 'actions'],
      currentPage: 1,
      perPage: 5,
      totalRows: 0,
      you: null,
      is_admin: false,
      message: '',
      loader: '/img/loader.gif',
      is_loading: false,
      showMessage: false,
      dismissSecs: 7,
      filterUsers: '',
      filterDate: '',
      dismissCountDown: 0,
      date_format: '',
      showDismissibleAlert: false
    };
  },
  methods: {
    reportLink: function reportLink(id) {
      return "/reports/".concat(id.toString());
    },
    viewReport: function viewReport(id) {
      var userLink = this.reportLink(id);
      this.$router.push({
        path: userLink
      });
    },
    countDownChanged: function countDownChanged(dismissCountDown) {
      this.dismissCountDown = dismissCountDown;
    },
    showAlert: function showAlert() {
      this.dismissCountDown = this.dismissSecs;
    },
    getReports: function getReports() {
      var url = '/api/daily-report?token=' + localStorage.getItem("api_token");

      if (this.filterDate) {
        url = url + '&date=' + this.filterDate;
      }

      if (this.filterUsers) {
        url = url + '&user=' + this.filterUsers;
      }

      var that = this;
      jQuery('#reports_table').DataTable({
        "lengthMenu": [[10, 50, 100, -1], [10, 50, 100, "All"]],
        processing: true,
        serverSide: true,
        "order": [],
        ajax: url,
        columns: [// {data: 'DT_RowIndex' },
        {
          data: 'name'
        }, {
          data: 'report',
          name: 'report',
          render: function render(data, type, full, meta) {
            return '<a data="' + full.id + '" class="view-item-icon">' + full.report + '</a>';
          }
        }, {
          data: 'created_at',
          name: 'created_at',
          render: function render(data, type, full, meta) {
            return that.dateFormat(full.created_at);
          }
        }, {
          data: 'action',
          name: 'action',
          render: function render(data, type, full, meta) {
            var _inner = '<a data="' + full.id + '" class="view-item-icon dropdown-item"><img src="/img/view-icon.png" alt="view-icn"/> View</a>';

            var btns = '<div class="custom-list-dropdown"><div class="dropdown"><p class="dropdown-toggle pointer text-center" id="listrepos' + full.id + '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="/img/drop-icon.svg" /></p><div class="dropdown-menu" aria-labelledby="listrepos' + full.id + '">' + _inner + '</div></div></div>';
            return btns;
          },
          orderable: false,
          searchable: false
        }],
        "language": {
          "processing": '<div class="loader-image"><img src="' + that.loader + '"></div>'
        },
        "dom": '<"top table-search-flds d-flex align-items-center justify-content-between"fl>rt<"bottom table-paginater"ip><"clear">'
      }); // trigger click functions.

      jQuery(document).on('click', '.view-item-icon', function () {
        var id = this.getAttribute('data');
        that.viewReport(id);
      });
    },
    getUsers: function getUsers() {
      var self = this;
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('/api/report-users?token=' + localStorage.getItem("api_token")).then(function (response) {
        self.users = response.data;
      });
    },
    filterData: function filterData() {
      localStorage.setItem('reportfilDate', this.filterDate);
      localStorage.setItem('reportfilUsers', this.filterUsers);
      jQuery('#reports_table').DataTable().destroy();
      this.getReports();
    },
    dateFormat: function dateFormat($date) {
      // set date format.
      return moment__WEBPACK_IMPORTED_MODULE_1___default()($date).format(this.date_format);
    },
    resetFilter: function resetFilter() {
      this.filterDate = '', this.filterUsers = '';
      localStorage.removeItem('reportfilDate');
      localStorage.removeItem('reportfilUsers');
      jQuery('#reports_table').DataTable().destroy();
      this.getReports();
    }
  },
  mounted: function mounted() {
    if (localStorage.getItem("reportfilDate")) {
      this.filterDate = localStorage.getItem("reportfilDate");
    }

    if (localStorage.getItem("reportfilUsers")) {
      this.filterUsers = localStorage.getItem("reportfilUsers");
    }

    this.getReports();
    this.getUsers();
    this.is_admin = localStorage.getItem("type");
    this.date_format = localStorage.getItem("date_format");
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/reports/Reports.vue?vue&type=template&id=5d5a2596&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/reports/Reports.vue?vue&type=template&id=5d5a2596& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CRow",
    [
      _c(
        "CCol",
        { attrs: { col: "12", xl: "12" } },
        [
          _c(
            "transition",
            { attrs: { name: "slide" } },
            [
              _c(
                "CCard",
                [
                  _c(
                    "CCardHeader",
                    {
                      staticClass:
                        "d-flex justify-content-between align-items-center"
                    },
                    [_c("h4", { staticClass: "mb-0" }, [_vm._v("Reports")])]
                  ),
                  _vm._v(" "),
                  _c(
                    "CCardBody",
                    [
                      _c(
                        "CAlert",
                        {
                          attrs: {
                            show: _vm.dismissCountDown,
                            color: "primary",
                            fade: ""
                          },
                          on: {
                            "update:show": function($event) {
                              _vm.dismissCountDown = $event
                            }
                          }
                        },
                        [
                          _vm._v(
                            "\n            " +
                              _vm._s(_vm.message) +
                              "\n          "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _vm.is_admin == "true"
                        ? _c(
                            "CRow",
                            { staticClass: "table-filters mb-3" },
                            [
                              _c("CCol", [
                                _c(
                                  "select",
                                  {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.filterUsers,
                                        expression: "filterUsers"
                                      }
                                    ],
                                    staticClass: "form-control mb-0",
                                    on: {
                                      change: [
                                        function($event) {
                                          var $$selectedVal = Array.prototype.filter
                                            .call(
                                              $event.target.options,
                                              function(o) {
                                                return o.selected
                                              }
                                            )
                                            .map(function(o) {
                                              var val =
                                                "_value" in o
                                                  ? o._value
                                                  : o.value
                                              return val
                                            })
                                          _vm.filterUsers = $event.target
                                            .multiple
                                            ? $$selectedVal
                                            : $$selectedVal[0]
                                        },
                                        _vm.filterData
                                      ]
                                    }
                                  },
                                  [
                                    _c("option", { attrs: { value: "" } }, [
                                      _vm._v("--Select User --")
                                    ]),
                                    _vm._v(" "),
                                    _vm._l(_vm.users, function(user) {
                                      return _c(
                                        "option",
                                        { domProps: { value: user.id } },
                                        [_vm._v(_vm._s(user.name))]
                                      )
                                    })
                                  ],
                                  2
                                )
                              ]),
                              _vm._v(" "),
                              _c(
                                "CCol",
                                [
                                  _c("CInput", {
                                    staticClass: "mb-0",
                                    attrs: {
                                      placeholder: "Select Date",
                                      type: "date"
                                    },
                                    on: { change: _vm.filterData },
                                    model: {
                                      value: _vm.filterDate,
                                      callback: function($$v) {
                                        _vm.filterDate = $$v
                                      },
                                      expression: "filterDate"
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "CButton",
                                {
                                  staticClass: "float-right",
                                  attrs: { color: "primary" },
                                  on: {
                                    click: function($event) {
                                      return _vm.resetFilter()
                                    }
                                  }
                                },
                                [_vm._v("Reset")]
                              )
                            ],
                            1
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _c("div", { staticClass: "table-responsive" }, [
                        _c(
                          "table",
                          {
                            staticClass:
                              "table table-manage-user table-striped",
                            attrs: { id: "reports_table" }
                          },
                          [
                            _c("thead", [
                              _c("tr", [
                                _c("th", { attrs: { scope: "col" } }, [
                                  _vm._v("Name")
                                ]),
                                _vm._v(" "),
                                _c("th", { attrs: { scope: "col" } }, [
                                  _vm._v("Report")
                                ]),
                                _vm._v(" "),
                                _c("th", { attrs: { scope: "col" } }, [
                                  _vm._v("Report Date")
                                ]),
                                _vm._v(" "),
                                _c("th", { attrs: { scope: "col" } }, [
                                  _vm._v("Action")
                                ])
                              ])
                            ]),
                            _vm._v(" "),
                            _c("tbody")
                          ]
                        )
                      ])
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);