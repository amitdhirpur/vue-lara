<?php 
$api_url = 'https://edec5d2310cd3d443776b0b4bbca506d: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@damrakgin-dev.myshopify.com';
$products_obj_url = $api_url.'/admin/products.json';
$products_content = @file_get_contents( $products_obj_url );
$products_json = json_decode( $products_content, true );
$products = $products_json['products'];
$product = $products[0];
?>   
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<style>
    .img-bottle img {
        max-height: 400px;
    }
    .cart-section {
        padding: 40px 0px;
        border-bottom: 10px solid #FFFFFF;
        -moz-border-image: -moz-linear-gradient(left, #fff200 0%, #f93943 50%, #b5238e 100%);
        -webkit-border-image: -webkit-linear-gradient(left, #fff200 0%, #f93943 50%, #b5238e 100%);
        border-image: linear-gradient(to right, #fff200 0%, #f93943 50%, #b5238e 100%);
        border-image-slice: 1;
    }
    .qty-input {
        border-radius: 4px;
        box-shadow: 0 1em 2em -0.9em rgba(#000, 0.7);
        transform: scale(1.5);
    }
    img.img-cart {
        max-width: 150px;
        cursor:pointer;
    }
</style>
<section class="cart-section">
    <div class="container-fluid passoa-cocktail img-relative">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="text-center img-bottle"><img src="<?php echo $product['image']['src']; ?>"></div>
                </div>
                <div class="col-md-8">
                    <h1><?php echo  $product['title'] ?></h1>
                    <p><span>Price: </span>$<span class="product-price"><?php echo  $product['variants'][0]['price']; ?></span></p>
                    <p><span>Size:</span><span><select class="product-size" name="p_size">
                    <?php foreach($product['variants'] as $key => $variant) { ?><option value="<?php echo $variant['title']; ?>" data-price="<?php echo $variant['price']; ?>" <?php if ($key==0){ echo 'selected'; } ?> data-id="<?php echo $variant['id']; ?>"><?php echo $variant['title']; ?></option> <?php } ?></select></span>
                    <span>Qty:</span><span class="qty-input">
                    <button class="qty-count qty-count--minus" data-action="minus" type="button">-</button>
                    <input class="product-qty" type="number" name="product-qty" min="1" max="10" value="1">
                    <button class="qty-count qty-count--add" data-action="add" type="button">+</button></span></p>
                    <img class="img-cart" src="https://dev.team.teqtop.com/add.png">
                </div>
            </div>
        </div>
    </div>
</section>
<script>
  var QtyInput = (function () {
    var $qtyInputs = $(".qty-input");
    if (!$qtyInputs.length) {
        return;
    }
    var $inputs = $qtyInputs.find(".product-qty");
    var $countBtn = $qtyInputs.find(".qty-count");
    var qtyMin = parseInt($inputs.attr("min"));
    var qtyMax = parseInt($inputs.attr("max"));
    $inputs.change(function () {
        var $this = $(this);
        var $minusBtn = $this.siblings(".qty-count--minus");
        var $addBtn = $this.siblings(".qty-count--add");
        var qty = parseInt($this.val());

        if (isNaN(qty) || qty <= qtyMin) {
            $this.val(qtyMin);
            $minusBtn.attr("disabled", true);
        } else {
            $minusBtn.attr("disabled", false);

            if(qty >= qtyMax){
                $this.val(qtyMax);
                $addBtn.attr('disabled', true);
            } else {
                $this.val(qty);
                $addBtn.attr('disabled', false);
            }
        }
    });
    $countBtn.click(function () {
        var operator = this.dataset.action;
        var $this = $(this);
        var $input = $this.siblings(".product-qty");
        var qty = parseInt($input.val());
        if (operator == "add") {
            qty += 1;
            if (qty >= qtyMin + 1) {
                $this.siblings(".qty-count--minus").attr("disabled", false);
            }
            if (qty >= qtyMax) {
                $this.attr("disabled", true);
            }
        } else {
            qty = qty <= qtyMin ? qtyMin : (qty -= 1);
            if (qty == qtyMin) {
                $this.attr("disabled", true);
            }
            if (qty < qtyMax) {
                $this.siblings(".qty-count--add").attr("disabled", false);
            }
        }
        $input.val(qty);
    });
})();
jQuery(document).on('change', '.product-size', function() {
    jQuery('.product-price').html(jQuery(this).find(":selected").attr('data-price'))
});
jQuery(document).on('click', '.img-cart', function() {
	let headers = new Headers();
	var _id = jQuery(".product-size option:selected").attr('data-id');
	var _qty = jQuery(".product-qty").val();
    let formData = {
    	 'items': [{
    	  'id': _id,
    	  'quantity': _qty
    	}]
	};
    debugger    
    // headers.append('Content-Type', 'application/json');
    // headers.append('Access-Control-Allow-Origin', 'https://dev.team.teqtop.com');
    // headers.append('X-Shopify-Access-Token', '358575371ae8557eae0ce572101fdef3');
    // headers.append('X-Shop-Domain', 'https://damrakgin-dev.myshopify.com');
    // fetch('https://edec5d2310cd3d443776b0b4bbca506d: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@damrakgin-dev.myshopify.com/admin/products/count.json', {
    //        method: 'POST',
    //        mode: 'no-cors',
    //        headers: headers,
    //        dataType: 'json', 
    //        body: JSON.stringify(formData)
    //    })
    //    .then(response => {
    //  debugger
    // })
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Credentials','true');
    headers.append('Access-Control-Allow-Methods','GET');
    headers.append('Access-Control-Allow-Headers','content-type');
    headers.append('Content-Type','application/json');
    $.ajax({
          type:'GET',
          dataType: 'json',
          mode: 'no-cors',
          headers: headers,
          url:'https://edec5d2310cd3d443776b0b4bbca506d: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@damrakgin-dev.myshopify.com/admin/products/count.json',
          success:function(data) {
             debugger
          }
    });
});
  </script>