(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[49],{

/***/ "../coreui/src/views/projects/Project.vue":
/*!************************************************!*\
  !*** ../coreui/src/views/projects/Project.vue ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Project_vue_vue_type_template_id_aeb03024___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Project.vue?vue&type=template&id=aeb03024& */ "../coreui/src/views/projects/Project.vue?vue&type=template&id=aeb03024&");
/* harmony import */ var _Project_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Project.vue?vue&type=script&lang=js& */ "../coreui/src/views/projects/Project.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Project_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Project_vue_vue_type_template_id_aeb03024___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Project_vue_vue_type_template_id_aeb03024___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/projects/Project.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/projects/Project.vue?vue&type=script&lang=js&":
/*!*************************************************************************!*\
  !*** ../coreui/src/views/projects/Project.vue?vue&type=script&lang=js& ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Project_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./Project.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/projects/Project.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Project_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/projects/Project.vue?vue&type=template&id=aeb03024&":
/*!*******************************************************************************!*\
  !*** ../coreui/src/views/projects/Project.vue?vue&type=template&id=aeb03024& ***!
  \*******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Project_vue_vue_type_template_id_aeb03024___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./Project.vue?vue&type=template&id=aeb03024& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/projects/Project.vue?vue&type=template&id=aeb03024&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Project_vue_vue_type_template_id_aeb03024___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Project_vue_vue_type_template_id_aeb03024___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/projects/Project.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/projects/Project.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "../coreui/node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! devextreme-vue/html-editor */ "../coreui/node_modules/devextreme-vue/html-editor.js");
/* harmony import */ var devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuex */ "../coreui/node_modules/vuex/dist/vuex.esm.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Project',
  data: function data() {
    return {
      access_detail: '',
      items: [],
      fields: [{
        key: 'key'
      }, {
        key: 'value'
      }],
      is_admin: false,
      current_user: ''
    };
  },
  components: {
    DxHtmlEditor: devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_1__["DxHtmlEditor"],
    DxToolbar: devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_1__["DxToolbar"],
    DxItem: devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_1__["DxItem"]
  },
  methods: {
    goBack: function goBack() {
      this.$router.back();
    },
    edit: function edit() {
      var editClass = document.querySelector('.edit-detail');
      var targetClass = document.querySelector('.target-detail');

      if (editClass.classList.contains('hide')) {
        editClass.classList.remove('hide');
      } else {
        editClass.classList.add('hide');
      }

      if (targetClass.classList.contains('hide')) {
        targetClass.classList.remove('hide');
      } else {
        targetClass.classList.add('hide');
      }
    },
    save: function save() {
      var self = this;
      self.$store.dispatch('project/saveAccessDetail', {
        id: self.$route.params.id,
        access_detail: self.access_detail
      }).then(function () {
        self.access_detail = self.$store.state.project.access_detail;
        self.edit();
      });
    },
    goToTask: function goToTask(id) {
      var _id = btoa(id.toString());

      var link = "/tasks?project=".concat(_id);
      this.$router.push({
        path: link
      });
    },
    shouldDisplay: function shouldDisplay(item) {
      this.current_user = localStorage.getItem("user");
      return this.is_admin == "true" || JSON.parse(this.current_user) == item.user_id;
    },
    goToDrive: function goToDrive(id) {
      var _id = btoa(id.toString());

      var link = "/drives/".concat(_id);
      this.$router.push({
        path: link
      });
    },
    goToUrl: function goToUrl(val) {
      var url = '';

      try {
        var url = new URL(val);
      } catch (_) {
        this.$swal('Warning', 'Url must be http/https.', 'warning');
      }

      if (url.protocol === "http:" || url.protocol === "https:") {
        window.open(url, '_blank');
      } else {
        this.$swal('Warning', 'Url must be http/https.', 'warning');
      }
    },
    goToEditProject: function goToEditProject(id) {
      var link = "/projects/".concat(id, "/edit");
      this.$router.push({
        path: link
      });
    }
  },
  mounted: function mounted() {
    var self = this;
    axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('/api/projects/' + self.$route.params.id + '?token=' + localStorage.getItem("api_token")).then(function (response) {
      self.items = response.data;
      self.access_detail = response.data.access_detail;
    })["catch"](function (error) {
      console.log(error);
      self.$router.push({
        path: '/login'
      });
    });
    this.is_admin = localStorage.getItem("type");
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/projects/Project.vue?vue&type=template&id=aeb03024&":
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/projects/Project.vue?vue&type=template&id=aeb03024& ***!
  \*************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CRow",
    [
      _c(
        "CCol",
        { attrs: { col: "12", lg: "12" } },
        [
          _c(
            "CCard",
            [
              _c("CCardHeader", { staticClass: "text-center" }, [
                _c("h3", [_vm._v(_vm._s(_vm.items.name))]),
                _vm._v(" "),
                _c(
                  "a",
                  {
                    staticClass: "pointer",
                    on: {
                      click: function($event) {
                        return _vm.goToUrl(_vm.items.url)
                      }
                    }
                  },
                  [_vm._v(_vm._s(_vm.items.url))]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "btn-back-drive d-flex justify-content-end" },
                  [
                    _vm.shouldDisplay(_vm.items)
                      ? _c(
                          "CButton",
                          {
                            staticClass: "mr-1",
                            attrs: { color: "primary" },
                            on: {
                              click: function($event) {
                                return _vm.goToEditProject(_vm.items.id)
                              }
                            }
                          },
                          [
                            _c("CIcon", { attrs: { name: "cil-pencil" } }),
                            _c("span", [_vm._v(" Edit")])
                          ],
                          1
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    _c(
                      "CButton",
                      {
                        staticClass: "mr-1",
                        attrs: { color: "primary" },
                        on: {
                          click: function($event) {
                            return _vm.goToTask(_vm.items.id)
                          }
                        }
                      },
                      [
                        _c("CIcon", { attrs: { name: "cil-task" } }),
                        _c("span", [_vm._v(" Tasks")])
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "CButton",
                      {
                        staticClass: "mr-1",
                        attrs: { color: "primary" },
                        on: {
                          click: function($event) {
                            return _vm.goToDrive(_vm.items.folder_id)
                          }
                        }
                      },
                      [
                        _c("CIcon", { attrs: { name: "cil-folder" } }),
                        _c("span", [_vm._v(" Drive")])
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "CButton",
                      {
                        attrs: { color: "primary" },
                        on: { click: _vm.goBack }
                      },
                      [_vm._v("Back")]
                    )
                  ],
                  1
                )
              ]),
              _vm._v(" "),
              _c(
                "CCardBody",
                [
                  _c(
                    "CRow",
                    [
                      _c(
                        "CCol",
                        { attrs: { col: "12", lg: "12" } },
                        [
                          _c("h4", [_vm._v("Access Detail")]),
                          _vm._v(" "),
                          _c(
                            "CRow",
                            [
                              _c("CCol", { attrs: { col: "12", lg: "12" } }, [
                                _c(
                                  "span",
                                  {
                                    staticClass:
                                      "edit-btn cursor-pointer float-right",
                                    on: { click: _vm.edit }
                                  },
                                  [
                                    _c("CIcon", {
                                      attrs: { name: "cil-pencil" }
                                    })
                                  ],
                                  1
                                )
                              ]),
                              _vm._v(" "),
                              _c("CCol", {
                                staticClass: "edit-detail",
                                attrs: { col: "12", lg: "12" },
                                domProps: {
                                  innerHTML: _vm._s(_vm.access_detail)
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "CCol",
                                {
                                  staticClass: "hide target-detail",
                                  attrs: { col: "12", lg: "12" }
                                },
                                [
                                  _c(
                                    "DxHtmlEditor",
                                    {
                                      ref: "textEditor",
                                      attrs: { height: "300px" },
                                      model: {
                                        value: _vm.access_detail,
                                        callback: function($$v) {
                                          _vm.access_detail = $$v
                                        },
                                        expression: "access_detail"
                                      }
                                    },
                                    [
                                      _c(
                                        "DxToolbar",
                                        [
                                          _c("DxItem", {
                                            attrs: { "format-name": "header" }
                                          }),
                                          _vm._v(" "),
                                          _c("DxItem", {
                                            attrs: { "format-name": "bold" }
                                          }),
                                          _vm._v(" "),
                                          _c("DxItem", {
                                            attrs: { "format-name": "italic" }
                                          }),
                                          _vm._v(" "),
                                          _c("DxItem", {
                                            attrs: {
                                              "format-name": "underline"
                                            }
                                          }),
                                          _vm._v(" "),
                                          _c("DxItem", {
                                            attrs: {
                                              "format-name": "separator"
                                            }
                                          }),
                                          _vm._v(" "),
                                          _c("DxItem", {
                                            attrs: {
                                              "format-name": "bulletList"
                                            }
                                          }),
                                          _vm._v(" "),
                                          _c("DxItem", {
                                            attrs: { "format-name": "link" }
                                          }),
                                          _vm._v(" "),
                                          _c("DxItem", {
                                            attrs: {
                                              "format-name": "blockquote"
                                            }
                                          }),
                                          _vm._v(" "),
                                          _c("DxItem", {
                                            attrs: {
                                              "format-name": "alignLeft"
                                            }
                                          }),
                                          _vm._v(" "),
                                          _c("DxItem", {
                                            attrs: {
                                              "format-name": "alignCenter"
                                            }
                                          }),
                                          _vm._v(" "),
                                          _c("DxItem", {
                                            attrs: {
                                              "format-name": "alignRight"
                                            }
                                          }),
                                          _vm._v(" "),
                                          _c("DxItem", {
                                            attrs: {
                                              "format-name": "alignJustify"
                                            }
                                          }),
                                          _vm._v(" "),
                                          _c("DxItem", {
                                            attrs: {
                                              "format-name": "separator"
                                            }
                                          }),
                                          _vm._v(" "),
                                          _c("DxItem", {
                                            attrs: { "format-name": "color" }
                                          }),
                                          _vm._v(" "),
                                          _c("DxItem", {
                                            attrs: {
                                              "format-name": "background"
                                            }
                                          }),
                                          _vm._v(" "),
                                          _c("DxItem", {
                                            attrs: { "format-name": "undo" }
                                          }),
                                          _vm._v(" "),
                                          _c("DxItem", {
                                            attrs: { "format-name": "redo" }
                                          })
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "CButton",
                                    {
                                      staticClass: "float-right",
                                      attrs: { color: "primary" },
                                      on: { click: _vm.save }
                                    },
                                    [_vm._v("save")]
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);