(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[64],{

/***/ "../coreui/src/views/Dashboard.vue":
/*!*****************************************!*\
  !*** ../coreui/src/views/Dashboard.vue ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Dashboard_vue_vue_type_template_id_78f2734c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Dashboard.vue?vue&type=template&id=78f2734c& */ "../coreui/src/views/Dashboard.vue?vue&type=template&id=78f2734c&");
/* harmony import */ var _Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Dashboard.vue?vue&type=script&lang=js& */ "../coreui/src/views/Dashboard.vue?vue&type=script&lang=js&");
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Dashboard_vue_vue_type_template_id_78f2734c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Dashboard_vue_vue_type_template_id_78f2734c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/Dashboard.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/Dashboard.vue?vue&type=script&lang=js&":
/*!******************************************************************!*\
  !*** ../coreui/src/views/Dashboard.vue?vue&type=script&lang=js& ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/Dashboard.vue?vue&type=script&lang=js&");
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "../coreui/src/views/Dashboard.vue?vue&type=template&id=78f2734c&":
/*!************************************************************************!*\
  !*** ../coreui/src/views/Dashboard.vue?vue&type=template&id=78f2734c& ***!
  \************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_78f2734c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=template&id=78f2734c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/Dashboard.vue?vue&type=template&id=78f2734c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_78f2734c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_78f2734c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/Dashboard.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/Dashboard.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/Dashboard.vue?vue&type=template&id=78f2734c&":
/*!******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/Dashboard.vue?vue&type=template&id=78f2734c& ***!
  \******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "dashboard-section" },
    [
      _c(
        "CRow",
        { staticClass: "remove-extrarow-padding" },
        [
          _c(
            "CCol",
            { attrs: { col: "8", lg: "8" } },
            [
              _vm.has_permission
                ? _c(
                    "CCard",
                    { staticClass: "feed-field-section" },
                    [
                      _c("CCardBody", [
                        _c(
                          "div",
                          { staticClass: "feed-textfield" },
                          [
                            _c(
                              "CRow",
                              [
                                _vm.show_editor
                                  ? _c(
                                      "CCol",
                                      { attrs: { col: "12" } },
                                      [
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "editor-module dashboard-feed-editor"
                                          },
                                          [
                                            _c(
                                              "DxHtmlEditor",
                                              {
                                                ref: "textEditor",
                                                attrs: {
                                                  mentions: _vm.mentions,
                                                  height: "300px"
                                                },
                                                model: {
                                                  value: _vm.feed,
                                                  callback: function($$v) {
                                                    _vm.feed = $$v
                                                  },
                                                  expression: "feed"
                                                }
                                              },
                                              [
                                                _c(
                                                  "DxToolbar",
                                                  [
                                                    _c("DxItem", {
                                                      attrs: {
                                                        "format-name": "bold"
                                                      }
                                                    }),
                                                    _vm._v(" "),
                                                    _c("DxItem", {
                                                      attrs: {
                                                        "format-name": "link"
                                                      }
                                                    }),
                                                    _vm._v(" "),
                                                    _c("DxItem", {
                                                      attrs: {
                                                        "format-name": "image"
                                                      }
                                                    }),
                                                    _vm._v(" "),
                                                    _c("DxItem", {
                                                      attrs: {
                                                        "format-name":
                                                          "blockquote"
                                                      }
                                                    })
                                                  ],
                                                  1
                                                )
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "editor-icons" },
                                              [
                                                _c(
                                                  "a",
                                                  {
                                                    staticClass:
                                                      "attachment-img-ico",
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.showDropbox(
                                                          $event
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _c("img", {
                                                      attrs: {
                                                        src:
                                                          "/img/attachment.png",
                                                        alt: ""
                                                      }
                                                    })
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "a",
                                                  {
                                                    staticClass: "bold-img-ico",
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.selectOption(
                                                          $event,
                                                          "bold"
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _c("img", {
                                                      attrs: {
                                                        src: "/img/bold.png",
                                                        alt: ""
                                                      }
                                                    })
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "a",
                                                  {
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.selectOption(
                                                          $event,
                                                          "link"
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _c("CIcon", {
                                                      attrs: {
                                                        name: "cil-link"
                                                      }
                                                    })
                                                  ],
                                                  1
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "a",
                                                  {
                                                    staticClass:
                                                      "quoted-img-ico",
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.selectOption(
                                                          $event,
                                                          "blockquote"
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _c("img", {
                                                      attrs: {
                                                        src: "/img/quote.png",
                                                        alt: ""
                                                      }
                                                    })
                                                  ]
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _vm.edit_name.length > 0
                                              ? _c(
                                                  "div",
                                                  {
                                                    staticClass: "files-names"
                                                  },
                                                  _vm._l(
                                                    _vm.edit_name,
                                                    function(name) {
                                                      return _c("p", [
                                                        _vm._v(
                                                          _vm._s(name.name) +
                                                            " "
                                                        ),
                                                        _c(
                                                          "a",
                                                          {
                                                            staticClass:
                                                              "float-right",
                                                            on: {
                                                              click: function(
                                                                $event
                                                              ) {
                                                                return _vm.removeIndex(
                                                                  name.file
                                                                )
                                                              }
                                                            }
                                                          },
                                                          [
                                                            _c("CIcon", {
                                                              attrs: {
                                                                name: "cil-x"
                                                              }
                                                            })
                                                          ],
                                                          1
                                                        )
                                                      ])
                                                    }
                                                  ),
                                                  0
                                                )
                                              : _vm._e(),
                                            _vm._v(" "),
                                            _vm.is_shown
                                              ? _c("vue-dropzone", {
                                                  ref: "imgDropZone",
                                                  attrs: {
                                                    id: "customdropzone",
                                                    options:
                                                      _vm.dropzoneOptions,
                                                    data_id: ""
                                                  },
                                                  on: {
                                                    "vdropzone-complete":
                                                      _vm.afterComplete,
                                                    "vdropzone-removed-file":
                                                      _vm.removeThisFile
                                                  }
                                                })
                                              : _vm._e()
                                          ],
                                          1
                                        ),
                                        _vm._v(" "),
                                        _vm.is_edit
                                          ? _c(
                                              "CButton",
                                              {
                                                attrs: { color: "primary" },
                                                on: { click: _vm.editFeedVal }
                                              },
                                              [_vm._v("Edit")]
                                            )
                                          : _c(
                                              "CButton",
                                              {
                                                attrs: { color: "primary" },
                                                on: { click: _vm.addFeed }
                                              },
                                              [_vm._v("Send")]
                                            ),
                                        _vm._v(" "),
                                        _c(
                                          "CButton",
                                          {
                                            attrs: { color: "secondary" },
                                            on: { click: _vm.showEditor }
                                          },
                                          [_vm._v("Cancel")]
                                        )
                                      ],
                                      1
                                    )
                                  : _c("CCol", { attrs: { col: "12" } }, [
                                      _c(
                                        "div",
                                        {
                                          staticClass: "dash-texting",
                                          on: { click: _vm.showEditor }
                                        },
                                        [_vm._v("Send Message ...")]
                                      )
                                    ])
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ])
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _c("div", { staticClass: "dashboard-heading" }, [
                _c("h3", [_vm._v("Feeds")]),
                _vm._v(" "),
                _c("div", { staticClass: "input-group" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.search,
                        expression: "search"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: {
                      type: "text",
                      placeholder: "Filter & Search",
                      "aria-label": "Filter & Search"
                    },
                    domProps: { value: _vm.search },
                    on: {
                      input: [
                        function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.search = $event.target.value
                        },
                        function($event) {
                          return _vm.makeSearch("search")
                        }
                      ]
                    }
                  }),
                  _vm._v(" "),
                  _c("div", { staticClass: "input-group-append" }, [
                    _c(
                      "span",
                      {
                        staticClass: "input-group-text",
                        on: {
                          click: function($event) {
                            return _vm.makeSearch("search")
                          }
                        }
                      },
                      [
                        _c("CIcon", { attrs: { name: "cil-magnifying-glass" } })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "span",
                      {
                        staticClass: "input-group-text",
                        on: { click: _vm.emptySearch }
                      },
                      [_c("CIcon", { attrs: { name: "cil-x" } })],
                      1
                    )
                  ])
                ])
              ]),
              _vm._v(" "),
              _c(
                "CCard",
                [
                  _c("CCardBody", [
                    _c(
                      "div",
                      { staticClass: "feed-listing-area" },
                      [
                        _vm._l(_vm.feeds, function(item, key) {
                          return _c(
                            "div",
                            {
                              staticClass: "listing-section mt-3",
                              attrs: { data: key }
                            },
                            [
                              _c(
                                "div",
                                {
                                  key: key,
                                  class: "outer-sec make-quote-" + key
                                },
                                [
                                  _c(
                                    "CRow",
                                    [
                                      _c("CCol", { attrs: { col: "1" } }, [
                                        _c(
                                          "div",
                                          { staticClass: "small-rounded-img" },
                                          [
                                            item.user_profile
                                              ? _c("img", {
                                                  attrs: {
                                                    src: item.user_profile
                                                  }
                                                })
                                              : _c("img", {
                                                  attrs: { src: _vm.avtar }
                                                })
                                          ]
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c("CCol", { attrs: { col: "11" } }, [
                                        _c(
                                          "div",
                                          { staticClass: "feeds-listing" },
                                          [
                                            _c(
                                              "div",
                                              { staticClass: "feed-user-det" },
                                              [
                                                _c(
                                                  "div",
                                                  { staticClass: "inner-div" },
                                                  [
                                                    _c("h4", [
                                                      _vm._v(
                                                        _vm._s(item.user_name)
                                                      ),
                                                      _c("span", [
                                                        _vm._v(
                                                          " > To All Employees"
                                                        )
                                                      ])
                                                    ]),
                                                    _vm._v(" "),
                                                    _c("p", [
                                                      _vm._v(
                                                        _vm._s(
                                                          _vm.dateFormat(
                                                            item.created_at
                                                          )
                                                        )
                                                      )
                                                    ])
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "div",
                                                  { staticClass: "inner-div" },
                                                  [
                                                    _vm.shouldDisplay(item)
                                                      ? _c(
                                                          "CDropdown",
                                                          {
                                                            staticClass:
                                                              "custom-menu-btn",
                                                            attrs: {
                                                              "toggler-text":
                                                                "..."
                                                            }
                                                          },
                                                          [
                                                            _c(
                                                              "CDropdownItem",
                                                              {
                                                                on: {
                                                                  click: function(
                                                                    $event
                                                                  ) {
                                                                    return _vm.editFeed(
                                                                      item
                                                                    )
                                                                  }
                                                                }
                                                              },
                                                              [
                                                                _c("CIcon", {
                                                                  attrs: {
                                                                    name:
                                                                      "cil-pencil"
                                                                  }
                                                                }),
                                                                _vm._v(" Edit")
                                                              ],
                                                              1
                                                            ),
                                                            _vm._v(" "),
                                                            _c(
                                                              "CDropdownItem",
                                                              {
                                                                on: {
                                                                  click: function(
                                                                    $event
                                                                  ) {
                                                                    return _vm.deleteFeed(
                                                                      item.id
                                                                    )
                                                                  }
                                                                }
                                                              },
                                                              [
                                                                _c("CIcon", {
                                                                  attrs: {
                                                                    name:
                                                                      "cil-trash"
                                                                  }
                                                                }),
                                                                _vm._v(
                                                                  " Delete"
                                                                )
                                                              ],
                                                              1
                                                            )
                                                          ],
                                                          1
                                                        )
                                                      : _vm._e()
                                                  ],
                                                  1
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              {
                                                directives: [
                                                  {
                                                    name: "viewer",
                                                    rawName: "v-viewer"
                                                  }
                                                ],
                                                staticClass:
                                                  "description-sec images show-img-full"
                                              },
                                              [
                                                _c("p", {
                                                  domProps: {
                                                    innerHTML: _vm._s(
                                                      item.description
                                                    )
                                                  }
                                                })
                                              ]
                                            ),
                                            _vm._v(" "),
                                            JSON.parse(item.files)
                                              ? _c(
                                                  "div",
                                                  {
                                                    staticClass:
                                                      "images-viewer main-files-listing mt-3"
                                                  },
                                                  [
                                                    _vm._l(
                                                      JSON.parse(item.files),
                                                      function(src) {
                                                        return !_vm.isValidImageURL(
                                                          src
                                                        )
                                                          ? _c(
                                                              "div",
                                                              {
                                                                staticClass:
                                                                  "images-listing pr-3 files-names"
                                                              },
                                                              [
                                                                _c(
                                                                  "a",
                                                                  {
                                                                    attrs: {
                                                                      href: src,
                                                                      download:
                                                                        "",
                                                                      target:
                                                                        "_blank"
                                                                    }
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      _vm._s(
                                                                        _vm.getValidName(
                                                                          src
                                                                        )
                                                                      )
                                                                    )
                                                                  ]
                                                                )
                                                              ]
                                                            )
                                                          : _vm._e()
                                                      }
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "viewer",
                                                      {
                                                        staticClass:
                                                          "d-flex viewer-outer show-img-full",
                                                        attrs: {
                                                          images: JSON.parse(
                                                            item.files
                                                          )
                                                        }
                                                      },
                                                      _vm._l(
                                                        JSON.parse(item.files),
                                                        function(src, a) {
                                                          return _vm.isValidImageURL(
                                                            src
                                                          )
                                                            ? _c(
                                                                "div",
                                                                {
                                                                  staticClass:
                                                                    "images-listing pr-3"
                                                                },
                                                                [
                                                                  _c("img", {
                                                                    key: a,
                                                                    attrs: {
                                                                      src: src
                                                                    }
                                                                  })
                                                                ]
                                                              )
                                                            : _vm._e()
                                                        }
                                                      ),
                                                      0
                                                    )
                                                  ],
                                                  2
                                                )
                                              : _vm._e(),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              {
                                                staticClass:
                                                  "more-icons d-flex align-items-center"
                                              },
                                              [
                                                _c(
                                                  "popper",
                                                  {
                                                    attrs: {
                                                      trigger: "hover",
                                                      options: {
                                                        placement: "top"
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _c(
                                                      "div",
                                                      { staticClass: "popper" },
                                                      [
                                                        _c(
                                                          "div",
                                                          {
                                                            staticClass:
                                                              "like-box"
                                                          },
                                                          [
                                                            _c(
                                                              "div",
                                                              {
                                                                staticClass:
                                                                  "like-inner"
                                                              },
                                                              [
                                                                _c(
                                                                  "div",
                                                                  {
                                                                    staticClass:
                                                                      "header-box-like"
                                                                  },
                                                                  [
                                                                    _c(
                                                                      "CIcon",
                                                                      {
                                                                        attrs: {
                                                                          name:
                                                                            "cil-thumb-up"
                                                                        }
                                                                      }
                                                                    ),
                                                                    _c(
                                                                      "span",
                                                                      {
                                                                        staticClass:
                                                                          "like_box_count"
                                                                      },
                                                                      [
                                                                        _vm._v(
                                                                          _vm._s(
                                                                            item.likes_count
                                                                          )
                                                                        )
                                                                      ]
                                                                    )
                                                                  ],
                                                                  1
                                                                ),
                                                                _vm._v(" "),
                                                                _c(
                                                                  "div",
                                                                  {
                                                                    staticClass:
                                                                      "likeuser-section-values likeuser-feed-list"
                                                                  },
                                                                  _vm._l(
                                                                    item.likeusers,
                                                                    function(
                                                                      luser
                                                                    ) {
                                                                      return _c(
                                                                        "div",
                                                                        [
                                                                          _c(
                                                                            "div",
                                                                            {
                                                                              staticClass:
                                                                                "popup-left"
                                                                            },
                                                                            [
                                                                              _c(
                                                                                "div",
                                                                                {
                                                                                  staticClass:
                                                                                    "small-rounded-img"
                                                                                },
                                                                                [
                                                                                  luser.profile
                                                                                    ? _c(
                                                                                        "img",
                                                                                        {
                                                                                          staticClass:
                                                                                            "pro-img",
                                                                                          attrs: {
                                                                                            src:
                                                                                              luser.profile
                                                                                          }
                                                                                        }
                                                                                      )
                                                                                    : _c(
                                                                                        "img",
                                                                                        {
                                                                                          staticClass:
                                                                                            "pro-img",
                                                                                          attrs: {
                                                                                            src:
                                                                                              _vm.avtar
                                                                                          }
                                                                                        }
                                                                                      )
                                                                                ]
                                                                              )
                                                                            ]
                                                                          ),
                                                                          _vm._v(
                                                                            " "
                                                                          ),
                                                                          _c(
                                                                            "div",
                                                                            {
                                                                              staticClass:
                                                                                "popup-right"
                                                                            },
                                                                            [
                                                                              _c(
                                                                                "b",
                                                                                [
                                                                                  _vm._v(
                                                                                    _vm._s(
                                                                                      luser.name
                                                                                    )
                                                                                  )
                                                                                ]
                                                                              )
                                                                            ]
                                                                          )
                                                                        ]
                                                                      )
                                                                    }
                                                                  ),
                                                                  0
                                                                )
                                                              ]
                                                            )
                                                          ]
                                                        )
                                                      ]
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "a",
                                                      {
                                                        class: item.liked_by
                                                          ? "like-btn top active"
                                                          : "like-btn top",
                                                        attrs: {
                                                          slot: "reference"
                                                        },
                                                        on: {
                                                          click: function(
                                                            $event
                                                          ) {
                                                            return _vm.makeLike(
                                                              item.id,
                                                              "feed"
                                                            )
                                                          }
                                                        },
                                                        slot: "reference"
                                                      },
                                                      [
                                                        _c("span", [
                                                          _vm._v(
                                                            _vm._s(
                                                              item.likes_count
                                                            )
                                                          )
                                                        ]),
                                                        _vm._v(" "),
                                                        _c("CIcon", {
                                                          attrs: {
                                                            name: "cil-thumb-up"
                                                          }
                                                        })
                                                      ],
                                                      1
                                                    )
                                                  ]
                                                )
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              {
                                                staticClass:
                                                  "comment-section-values comment-feed-list"
                                              },
                                              [
                                                _c(
                                                  "div",
                                                  {
                                                    staticClass:
                                                      "pervious-comm-btn"
                                                  },
                                                  [
                                                    item.comment_count
                                                      ? _c(
                                                          "a",
                                                          {
                                                            attrs: {
                                                              "data-count": _vm.totalComment(
                                                                item.comment_count
                                                              ),
                                                              "data-paginator":
                                                                "1"
                                                            },
                                                            on: {
                                                              click: function(
                                                                $event
                                                              ) {
                                                                return _vm.viewMore(
                                                                  item.id
                                                                )
                                                              }
                                                            }
                                                          },
                                                          [
                                                            _vm._v(
                                                              "Previous Comment "
                                                            ),
                                                            _c("span", [
                                                              _vm._v(
                                                                "(" +
                                                                  _vm._s(
                                                                    _vm.totalComment(
                                                                      item.comment_count
                                                                    )
                                                                  ) +
                                                                  ")"
                                                              )
                                                            ])
                                                          ]
                                                        )
                                                      : _c("a", [
                                                          _vm._v(
                                                            "Previous Comment "
                                                          ),
                                                          _c("span", [
                                                            _vm._v("(0)")
                                                          ])
                                                        ]),
                                                    _vm._v(" "),
                                                    _vm.loading
                                                      ? _c(
                                                          "div",
                                                          {
                                                            staticClass:
                                                              "pervious-comm_loader"
                                                          },
                                                          [
                                                            _c("img", {
                                                              attrs: {
                                                                src: _vm.loader
                                                              }
                                                            })
                                                          ]
                                                        )
                                                      : _vm._e()
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "div",
                                                  { staticClass: "view-more" },
                                                  [
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "starting-comment"
                                                      },
                                                      _vm._l(
                                                        item.comment_list
                                                          .slice()
                                                          .reverse(),
                                                        function(comment, j) {
                                                          return _c(
                                                            "CRow",
                                                            _vm._b(
                                                              {
                                                                key: j,
                                                                staticClass:
                                                                  "pt-3"
                                                              },
                                                              "CRow",
                                                              item.comment_list,
                                                              false
                                                            ),
                                                            [
                                                              _c(
                                                                "CCol",
                                                                {
                                                                  attrs: {
                                                                    col: "1"
                                                                  }
                                                                },
                                                                [
                                                                  _c(
                                                                    "div",
                                                                    {
                                                                      staticClass:
                                                                        "small-rounded-img"
                                                                    },
                                                                    [
                                                                      comment.profile
                                                                        ? _c(
                                                                            "img",
                                                                            {
                                                                              attrs: {
                                                                                src:
                                                                                  comment.profile
                                                                              }
                                                                            }
                                                                          )
                                                                        : _c(
                                                                            "img",
                                                                            {
                                                                              attrs: {
                                                                                src:
                                                                                  _vm.avtar
                                                                              }
                                                                            }
                                                                          )
                                                                    ]
                                                                  )
                                                                ]
                                                              ),
                                                              _vm._v(" "),
                                                              _c(
                                                                "CCol",
                                                                {
                                                                  attrs: {
                                                                    col: "11"
                                                                  }
                                                                },
                                                                [
                                                                  _c(
                                                                    "div",
                                                                    {
                                                                      staticClass:
                                                                        "wrapper-comment"
                                                                    },
                                                                    [
                                                                      _c("b", [
                                                                        _vm._v(
                                                                          _vm._s(
                                                                            comment.user_name
                                                                          ) +
                                                                            " "
                                                                        ),
                                                                        _c(
                                                                          "span",
                                                                          {
                                                                            staticClass:
                                                                              "comment-date"
                                                                          },
                                                                          [
                                                                            _vm._v(
                                                                              _vm._s(
                                                                                _vm.dateFormat(
                                                                                  comment.created_at
                                                                                )
                                                                              )
                                                                            )
                                                                          ]
                                                                        )
                                                                      ]),
                                                                      _vm._v(
                                                                        " "
                                                                      ),
                                                                      _c(
                                                                        "div",
                                                                        {
                                                                          directives: [
                                                                            {
                                                                              name:
                                                                                "viewer",
                                                                              rawName:
                                                                                "v-viewer"
                                                                            }
                                                                          ],
                                                                          staticClass:
                                                                            "comment-section-detail images show-img-full"
                                                                        },
                                                                        [
                                                                          _c(
                                                                            "p",
                                                                            {
                                                                              domProps: {
                                                                                innerHTML: _vm._s(
                                                                                  JSON.parse(
                                                                                    comment.comment
                                                                                  )
                                                                                )
                                                                              }
                                                                            }
                                                                          )
                                                                        ]
                                                                      ),
                                                                      _vm._v(
                                                                        " "
                                                                      ),
                                                                      _c(
                                                                        "div",
                                                                        {
                                                                          staticClass:
                                                                            "images-viewer"
                                                                        },
                                                                        [
                                                                          _vm._l(
                                                                            JSON.parse(
                                                                              comment.files
                                                                            ),
                                                                            function(
                                                                              src
                                                                            ) {
                                                                              return !_vm.isValidImageURL(
                                                                                src
                                                                              )
                                                                                ? _c(
                                                                                    "div",
                                                                                    {
                                                                                      staticClass:
                                                                                        "images-listing pr-3 files-names"
                                                                                    },
                                                                                    [
                                                                                      _c(
                                                                                        "a",
                                                                                        {
                                                                                          attrs: {
                                                                                            href: src,
                                                                                            download:
                                                                                              "",
                                                                                            target:
                                                                                              "_blank"
                                                                                          }
                                                                                        },
                                                                                        [
                                                                                          _vm._v(
                                                                                            _vm._s(
                                                                                              _vm.getValidName(
                                                                                                src
                                                                                              )
                                                                                            )
                                                                                          )
                                                                                        ]
                                                                                      )
                                                                                    ]
                                                                                  )
                                                                                : _vm._e()
                                                                            }
                                                                          ),
                                                                          _vm._v(
                                                                            " "
                                                                          ),
                                                                          _c(
                                                                            "viewer",
                                                                            {
                                                                              staticClass:
                                                                                "d-flex viewer-outer show-img-full",
                                                                              attrs: {
                                                                                images: JSON.parse(
                                                                                  comment.files
                                                                                )
                                                                              }
                                                                            },
                                                                            _vm._l(
                                                                              JSON.parse(
                                                                                comment.files
                                                                              ),
                                                                              function(
                                                                                src
                                                                              ) {
                                                                                return _vm.isValidImageURL(
                                                                                  src
                                                                                )
                                                                                  ? _c(
                                                                                      "div",
                                                                                      {
                                                                                        staticClass:
                                                                                          "images-listing pr-3"
                                                                                      },
                                                                                      [
                                                                                        _c(
                                                                                          "img",
                                                                                          {
                                                                                            attrs: {
                                                                                              src: src
                                                                                            }
                                                                                          }
                                                                                        )
                                                                                      ]
                                                                                    )
                                                                                  : _vm._e()
                                                                              }
                                                                            ),
                                                                            0
                                                                          )
                                                                        ],
                                                                        2
                                                                      )
                                                                    ]
                                                                  ),
                                                                  _vm._v(" "),
                                                                  _c(
                                                                    "div",
                                                                    {
                                                                      staticClass:
                                                                        "wrapper-opt"
                                                                    },
                                                                    [
                                                                      _vm.shouldDisplayComm(
                                                                        comment
                                                                      )
                                                                        ? _c(
                                                                            "CDropdown",
                                                                            {
                                                                              staticClass:
                                                                                "custom-menu-btn",
                                                                              attrs: {
                                                                                "toggler-text":
                                                                                  "More"
                                                                              }
                                                                            },
                                                                            [
                                                                              _c(
                                                                                "CDropdownItem",
                                                                                {
                                                                                  on: {
                                                                                    click: function(
                                                                                      $event
                                                                                    ) {
                                                                                      return _vm.deleteComment(
                                                                                        comment.id,
                                                                                        item.id,
                                                                                        key
                                                                                      )
                                                                                    }
                                                                                  }
                                                                                },
                                                                                [
                                                                                  _c(
                                                                                    "CIcon",
                                                                                    {
                                                                                      attrs: {
                                                                                        name:
                                                                                          "cil-trash"
                                                                                      }
                                                                                    }
                                                                                  ),
                                                                                  _vm._v(
                                                                                    " Delete"
                                                                                  )
                                                                                ],
                                                                                1
                                                                              )
                                                                            ],
                                                                            1
                                                                          )
                                                                        : _vm._e()
                                                                    ],
                                                                    1
                                                                  )
                                                                ]
                                                              )
                                                            ],
                                                            1
                                                          )
                                                        }
                                                      ),
                                                      1
                                                    )
                                                  ]
                                                )
                                              ]
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "feed-comment-section pt-3"
                                          },
                                          [
                                            _c(
                                              "div",
                                              { staticClass: "comment-field" },
                                              [
                                                _c(
                                                  "a",
                                                  {
                                                    class:
                                                      "commentfiels-" + key,
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.showCommentEditor(
                                                          key
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [_vm._v("Add Comment")]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "div",
                                                  {
                                                    class:
                                                      "hide commenteditor-" +
                                                      key
                                                  },
                                                  [
                                                    _c(
                                                      "div",
                                                      {
                                                        class:
                                                          "editor-module editor-mod-" +
                                                          key
                                                      },
                                                      [
                                                        _c(
                                                          "DxHtmlEditor",
                                                          {
                                                            ref:
                                                              "commentEditor",
                                                            refInFor: true,
                                                            attrs: {
                                                              mentions:
                                                                _vm.mentions,
                                                              height: "100px"
                                                            },
                                                            model: {
                                                              value:
                                                                _vm
                                                                  .comment_editor[
                                                                  key
                                                                ],
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  _vm.comment_editor,
                                                                  key,
                                                                  $$v
                                                                )
                                                              },
                                                              expression:
                                                                "comment_editor[key]"
                                                            }
                                                          },
                                                          [
                                                            _c(
                                                              "DxToolbar",
                                                              [
                                                                _c("DxItem", {
                                                                  attrs: {
                                                                    "format-name":
                                                                      "bold"
                                                                  }
                                                                }),
                                                                _vm._v(" "),
                                                                _c("DxItem", {
                                                                  attrs: {
                                                                    "format-name":
                                                                      "link"
                                                                  }
                                                                }),
                                                                _vm._v(" "),
                                                                _c("DxItem", {
                                                                  attrs: {
                                                                    "format-name":
                                                                      "image"
                                                                  }
                                                                }),
                                                                _vm._v(" "),
                                                                _c("DxItem", {
                                                                  attrs: {
                                                                    "format-name":
                                                                      "blockquote"
                                                                  }
                                                                })
                                                              ],
                                                              1
                                                            )
                                                          ],
                                                          1
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "div",
                                                          {
                                                            staticClass:
                                                              "editor-icons"
                                                          },
                                                          [
                                                            _c(
                                                              "a",
                                                              {
                                                                staticClass:
                                                                  "attachment-img-ico",
                                                                on: {
                                                                  click: function(
                                                                    $event
                                                                  ) {
                                                                    return _vm.showCommDropbox(
                                                                      key
                                                                    )
                                                                  }
                                                                }
                                                              },
                                                              [
                                                                _c("img", {
                                                                  attrs: {
                                                                    src:
                                                                      "/img/attachment.png",
                                                                    alt: ""
                                                                  }
                                                                })
                                                              ]
                                                            ),
                                                            _vm._v(" "),
                                                            _c(
                                                              "a",
                                                              {
                                                                staticClass:
                                                                  "bold-img-ico",
                                                                on: {
                                                                  click: function(
                                                                    $event
                                                                  ) {
                                                                    return _vm.selectCommOption(
                                                                      key,
                                                                      "bold"
                                                                    )
                                                                  }
                                                                }
                                                              },
                                                              [
                                                                _c("img", {
                                                                  attrs: {
                                                                    src:
                                                                      "/img/bold.png",
                                                                    alt: ""
                                                                  }
                                                                })
                                                              ]
                                                            ),
                                                            _vm._v(" "),
                                                            _c(
                                                              "a",
                                                              {
                                                                on: {
                                                                  click: function(
                                                                    $event
                                                                  ) {
                                                                    return _vm.selectCommOption(
                                                                      key,
                                                                      "link"
                                                                    )
                                                                  }
                                                                }
                                                              },
                                                              [
                                                                _c("CIcon", {
                                                                  attrs: {
                                                                    name:
                                                                      "cil-link"
                                                                  }
                                                                })
                                                              ],
                                                              1
                                                            ),
                                                            _vm._v(" "),
                                                            _c(
                                                              "a",
                                                              {
                                                                staticClass:
                                                                  "quoted-img-ico",
                                                                on: {
                                                                  click: function(
                                                                    $event
                                                                  ) {
                                                                    return _vm.selectCommOption(
                                                                      key,
                                                                      "blockquote"
                                                                    )
                                                                  }
                                                                }
                                                              },
                                                              [
                                                                _c("img", {
                                                                  attrs: {
                                                                    src:
                                                                      "/img/quote.png",
                                                                    alt: ""
                                                                  }
                                                                })
                                                              ]
                                                            )
                                                          ]
                                                        ),
                                                        _vm._v(" "),
                                                        _c("vue-dropzone", {
                                                          ref: "commDropZone",
                                                          refInFor: true,
                                                          class:
                                                            "hide cust-dropzone-" +
                                                            key,
                                                          attrs: {
                                                            id:
                                                              "customdropzone" +
                                                              key,
                                                            options:
                                                              _vm.dropzoneOptions,
                                                            data_id: item.id
                                                          },
                                                          on: {
                                                            "vdropzone-complete":
                                                              _vm.afterComplete,
                                                            "vdropzone-removed-file":
                                                              _vm.removeThisFile
                                                          }
                                                        })
                                                      ],
                                                      1
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "CButton",
                                                      {
                                                        attrs: {
                                                          color: "primary"
                                                        },
                                                        on: {
                                                          click: function(
                                                            $event
                                                          ) {
                                                            return _vm.addComment(
                                                              key,
                                                              item.id
                                                            )
                                                          }
                                                        }
                                                      },
                                                      [_vm._v("Send")]
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "CButton",
                                                      {
                                                        attrs: {
                                                          color: "secondary"
                                                        },
                                                        on: {
                                                          click: function(
                                                            $event
                                                          ) {
                                                            return _vm.hideCommentEditor(
                                                              key
                                                            )
                                                          }
                                                        }
                                                      },
                                                      [_vm._v("Cancel")]
                                                    )
                                                  ],
                                                  1
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      ])
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ]
                          )
                        }),
                        _vm._v(" "),
                        _vm.feed_ct > 0
                          ? _c(
                              "div",
                              {
                                staticClass: "load-more pt-3 text-center pb-3"
                              },
                              [
                                _c(
                                  "CButton",
                                  {
                                    attrs: { color: "secondary" },
                                    on: { click: _vm.loadMore }
                                  },
                                  [_vm._v("Load More...")]
                                )
                              ],
                              1
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.feeds.length <= 0
                          ? _c("div", { staticClass: "no-feeds-sec" }, [
                              _c("a", { staticClass: "error-msg-ico" }, [
                                _c("img", { attrs: { src: "/img/error.png" } })
                              ]),
                              _vm._v(
                                " Currently there are no feeds.\n\t      \t\t\t\t\t"
                              )
                            ])
                          : _vm._e()
                      ],
                      2
                    )
                  ])
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "CCol",
            { attrs: { col: "4", lg: "4" } },
            [
              _c("DashboardSidebar", {
                attrs: {
                  data: _vm.sidebar_data,
                  permissions: _vm.has_permission,
                  birthdays: _vm.birthdays,
                  avtar: _vm.avtar
                }
              })
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _vm.latestComment
        ? _c(
            "div",
            {
              staticStyle: { display: "none" },
              attrs: { id: "record-latest-comment" }
            },
            [
              _c("div", { staticClass: "record-latest-comment" }, [
                _c(
                  "div",
                  { staticClass: "latest-comment" },
                  [
                    _c(
                      "CRow",
                      _vm._b(
                        { staticClass: "pt-3" },
                        "CRow",
                        _vm.latestComment,
                        false
                      ),
                      [
                        _c("CCol", { attrs: { col: "1" } }, [
                          _c("div", { staticClass: "small-rounded-img" }, [
                            _vm.latestComment.profile
                              ? _c("img", {
                                  attrs: { src: _vm.latestComment.profile }
                                })
                              : _c("img", { attrs: { src: _vm.avtar } })
                          ])
                        ]),
                        _vm._v(" "),
                        _c("CCol", { attrs: { col: "11" } }, [
                          _c("div", { staticClass: "wrapper-comment" }, [
                            _c("b", [
                              _vm._v(_vm._s(_vm.latestComment.user_name) + " "),
                              _c("span", { staticClass: "comment-date" }, [
                                _vm._v(
                                  _vm._s(
                                    _vm.dateFormat(_vm.latestComment.created_at)
                                  )
                                )
                              ])
                            ]),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                directives: [
                                  { name: "viewer", rawName: "v-viewer" }
                                ],
                                staticClass:
                                  "comment-section-detail images show-img-full"
                              },
                              [
                                _c("p", {
                                  domProps: {
                                    innerHTML: _vm._s(
                                      JSON.parse(_vm.latestComment.comment)
                                    )
                                  }
                                })
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "images-viewer" },
                              [
                                _vm._l(
                                  JSON.parse(_vm.latestComment.files),
                                  function(src) {
                                    return !_vm.isValidImageURL(src)
                                      ? _c(
                                          "div",
                                          {
                                            staticClass:
                                              "images-listing pr-3 files-names"
                                          },
                                          [
                                            _c(
                                              "a",
                                              {
                                                attrs: {
                                                  href: src,
                                                  download: "",
                                                  target: "_blank"
                                                }
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(_vm.getValidName(src))
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      : _vm._e()
                                  }
                                ),
                                _vm._v(" "),
                                _c(
                                  "viewer",
                                  {
                                    staticClass:
                                      "d-flex viewer-outer show-img-full",
                                    attrs: {
                                      images: JSON.parse(
                                        _vm.latestComment.files
                                      )
                                    }
                                  },
                                  _vm._l(
                                    JSON.parse(_vm.latestComment.files),
                                    function(src) {
                                      return _vm.isValidImageURL(src)
                                        ? _c(
                                            "div",
                                            {
                                              staticClass: "images-listing pr-3"
                                            },
                                            [_c("img", { attrs: { src: src } })]
                                          )
                                        : _vm._e()
                                    }
                                  ),
                                  0
                                )
                              ],
                              2
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "wrapper-opt" }, [
                            _c("div", { staticClass: "dropdown" }, [
                              _c(
                                "p",
                                {
                                  staticClass:
                                    "dropdown-toggle pointer text-center",
                                  attrs: {
                                    id: "listDroper" + _vm.latestComment.id,
                                    "data-toggle": "dropdown",
                                    "aria-haspopup": "true",
                                    "aria-expanded": "false"
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n\t\t\t\t\t\t\t\t\tMore\n\t\t\t\t\t\t\t\t"
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  staticClass: "dropdown-menu",
                                  attrs: {
                                    "aria-labelledby":
                                      "listDroper" + _vm.latestComment.id
                                  }
                                },
                                [
                                  _c(
                                    "a",
                                    {
                                      staticClass:
                                        "driver-item-icon dropdown-item text-info",
                                      attrs: {
                                        data: _vm.latestComment.id,
                                        "data-component":
                                          _vm.latestComment.component_id
                                      }
                                    },
                                    [
                                      _c(
                                        "svg",
                                        {
                                          staticClass: "c-icon",
                                          attrs: {
                                            xmlns: "http://www.w3.org/2000/svg",
                                            viewBox: "0 0 24 24",
                                            role: "img"
                                          }
                                        },
                                        [
                                          _c("path", {
                                            attrs: {
                                              d:
                                                "M4.5 22.125c-0 0.003-0 0.006-0 0.008 0 0.613 0.494 1.11 1.105 1.116h12.79c0.612-0.006 1.105-0.504 1.105-1.117 0-0.003-0-0.006-0-0.009v0-15h-15zM6 8.625h12v13.125h-12z"
                                            }
                                          }),
                                          _c("path", {
                                            attrs: {
                                              d:
                                                "M7.875 10.125h1.5v9.375h-1.5v-9.375z"
                                            }
                                          }),
                                          _c("path", {
                                            attrs: {
                                              d:
                                                "M11.25 10.125h1.5v9.375h-1.5v-9.375z"
                                            }
                                          }),
                                          _c("path", {
                                            attrs: {
                                              d:
                                                "M14.625 10.125h1.5v9.375h-1.5v-9.375z"
                                            }
                                          }),
                                          _c("path", {
                                            attrs: {
                                              d:
                                                "M15.375 4.125v-2.25c0-0.631-0.445-1.125-1.013-1.125h-4.725c-0.568 0-1.013 0.494-1.013 1.125v2.25h-5.625v1.5h18v-1.5zM10.125 2.25h3.75v1.875h-3.75z"
                                            }
                                          })
                                        ]
                                      ),
                                      _vm._v(" Delete\n\t\t\t\t\t\t\t\t\t")
                                    ]
                                  )
                                ]
                              )
                            ])
                          ])
                        ])
                      ],
                      1
                    )
                  ],
                  1
                )
              ])
            ]
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.moreComment
        ? _c(
            "div",
            {
              staticStyle: { display: "none" },
              attrs: { id: "previous-comment" }
            },
            [
              _c(
                "div",
                { staticClass: "more-comment" },
                _vm._l(_vm.targetComment, function(tcomment, j) {
                  return _c(
                    "CRow",
                    { key: j, staticClass: "pt-3" },
                    [
                      _c("CCol", { attrs: { col: "1" } }, [
                        _c("div", { staticClass: "small-rounded-img" }, [
                          tcomment.profile
                            ? _c("img", { attrs: { src: tcomment.profile } })
                            : _c("img", { attrs: { src: _vm.avtar } })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("CCol", { attrs: { col: "11" } }, [
                        _c("div", { staticClass: "wrapper-comment" }, [
                          _c("b", [
                            _vm._v(_vm._s(tcomment.user_name) + " "),
                            _c("span", { staticClass: "comment-date" }, [
                              _vm._v(
                                _vm._s(_vm.dateFormat(tcomment.created_at))
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              directives: [
                                { name: "viewer", rawName: "v-viewer" }
                              ],
                              staticClass:
                                "comment-section-detail images show-img-full"
                            },
                            [
                              _c("p", {
                                domProps: {
                                  innerHTML: _vm._s(
                                    JSON.parse(tcomment.comment)
                                  )
                                }
                              })
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "images-viewer" },
                            [
                              _vm._l(JSON.parse(tcomment.files), function(src) {
                                return !_vm.isValidImageURL(src)
                                  ? _c(
                                      "div",
                                      {
                                        staticClass:
                                          "images-listing pr-3 files-names"
                                      },
                                      [
                                        _c(
                                          "a",
                                          {
                                            attrs: {
                                              href: src,
                                              download: "",
                                              target: "_blank"
                                            }
                                          },
                                          [
                                            _vm._v(
                                              _vm._s(_vm.getValidName(src))
                                            )
                                          ]
                                        )
                                      ]
                                    )
                                  : _vm._e()
                              }),
                              _vm._v(" "),
                              _c(
                                "viewer",
                                {
                                  staticClass:
                                    "d-flex viewer-outer show-img-full",
                                  attrs: { images: JSON.parse(tcomment.files) }
                                },
                                _vm._l(JSON.parse(tcomment.files), function(
                                  src
                                ) {
                                  return _vm.isValidImageURL(src)
                                    ? _c(
                                        "div",
                                        { staticClass: "images-listing pr-3" },
                                        [_c("img", { attrs: { src: src } })]
                                      )
                                    : _vm._e()
                                }),
                                0
                              )
                            ],
                            2
                          )
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "wrapper-opt" },
                          [
                            _vm.shouldDisplayComm(tcomment)
                              ? _c(
                                  "CDropdown",
                                  {
                                    staticClass: "custom-menu-btn",
                                    attrs: { "toggler-text": "More" }
                                  },
                                  [
                                    _c(
                                      "CDropdownItem",
                                      {
                                        on: {
                                          click: function($event) {
                                            return _vm.deleteComment(
                                              tcomment.id,
                                              tcomment.component_id
                                            )
                                          }
                                        }
                                      },
                                      [
                                        _c("CIcon", {
                                          attrs: { name: "cil-trash" }
                                        }),
                                        _vm._v(" Delete")
                                      ],
                                      1
                                    )
                                  ],
                                  1
                                )
                              : _vm._e()
                          ],
                          1
                        )
                      ])
                    ],
                    1
                  )
                }),
                1
              )
            ]
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.is_loading
        ? _c("div", { staticClass: "loader-image" }, [
            _c("img", { attrs: { src: _vm.loader } })
          ])
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);