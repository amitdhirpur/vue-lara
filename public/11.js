(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[11],{

/***/ "../coreui/src/views/employees/CreateEmployee.vue":
/*!********************************************************!*\
  !*** ../coreui/src/views/employees/CreateEmployee.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CreateEmployee_vue_vue_type_template_id_7309833e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CreateEmployee.vue?vue&type=template&id=7309833e& */ "../coreui/src/views/employees/CreateEmployee.vue?vue&type=template&id=7309833e&");
/* harmony import */ var _CreateEmployee_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CreateEmployee.vue?vue&type=script&lang=js& */ "../coreui/src/views/employees/CreateEmployee.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CreateEmployee_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CreateEmployee_vue_vue_type_template_id_7309833e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CreateEmployee_vue_vue_type_template_id_7309833e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/employees/CreateEmployee.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/employees/CreateEmployee.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ../coreui/src/views/employees/CreateEmployee.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateEmployee_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./CreateEmployee.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/CreateEmployee.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateEmployee_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/employees/CreateEmployee.vue?vue&type=template&id=7309833e&":
/*!***************************************************************************************!*\
  !*** ../coreui/src/views/employees/CreateEmployee.vue?vue&type=template&id=7309833e& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateEmployee_vue_vue_type_template_id_7309833e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./CreateEmployee.vue?vue&type=template&id=7309833e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/CreateEmployee.vue?vue&type=template&id=7309833e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateEmployee_vue_vue_type_template_id_7309833e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateEmployee_vue_vue_type_template_id_7309833e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "../coreui/src/views/employees/validation-errors.vue":
/*!***********************************************************!*\
  !*** ../coreui/src/views/employees/validation-errors.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _validation_errors_vue_vue_type_template_id_2961bf1b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./validation-errors.vue?vue&type=template&id=2961bf1b& */ "../coreui/src/views/employees/validation-errors.vue?vue&type=template&id=2961bf1b&");
/* harmony import */ var _validation_errors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./validation-errors.vue?vue&type=script&lang=js& */ "../coreui/src/views/employees/validation-errors.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _validation_errors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _validation_errors_vue_vue_type_template_id_2961bf1b___WEBPACK_IMPORTED_MODULE_0__["render"],
  _validation_errors_vue_vue_type_template_id_2961bf1b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/employees/validation-errors.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/employees/validation-errors.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ../coreui/src/views/employees/validation-errors.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_validation_errors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./validation-errors.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/validation-errors.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_validation_errors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/employees/validation-errors.vue?vue&type=template&id=2961bf1b&":
/*!******************************************************************************************!*\
  !*** ../coreui/src/views/employees/validation-errors.vue?vue&type=template&id=2961bf1b& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_validation_errors_vue_vue_type_template_id_2961bf1b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./validation-errors.vue?vue&type=template&id=2961bf1b& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/validation-errors.vue?vue&type=template&id=2961bf1b&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_validation_errors_vue_vue_type_template_id_2961bf1b___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_validation_errors_vue_vue_type_template_id_2961bf1b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/CreateEmployee.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/employees/CreateEmployee.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "../coreui/node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _validation_errors_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./validation-errors.vue */ "../coreui/src/views/employees/validation-errors.vue");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuex */ "../coreui/node_modules/vuex/dist/vuex.esm.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'CreateEmployee',
  data: function data() {
    return {
      user: {
        name: '',
        test: '',
        email: '',
        password: '',
        employee_id: '',
        password_confirmation: '',
        joining_date: '',
        userRole: 0,
        position: 0
      },
      loader: '/img/loader.gif',
      is_loading: false,
      message: '',
      roles: [],
      items: [],
      dismissSecs: 7,
      dismissCountDown: 0,
      showDismissibleAlert: false
    };
  },
  components: {
    validationError: _validation_errors_vue__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapGetters"])(["errors"])),
  methods: {
    goBack: function goBack() {
      this.$router.go(-1);
    },
    validationError: function validationError() {
      var self = this;
      var error = {};

      if (!self.user.name) {
        error.name = [];
        error.name[0] = "Name Field is required!";
      }

      if (!self.user.employee_id) {
        error.employee_id = [];
        error.employee_id[0] = "Employee Field is required!";
      }

      if (!self.user.email) {
        error.email = [];
        error.email[0] = "Email Field is required!";
      }

      if (!self.user.joining_date) {
        error.joining_date = [];
        error.joining_date[0] = "Joining Date Field is required!";
      }

      if (!self.user.password) {
        error.password = [];
        error.password[0] = "Password Field is required!";
      }

      if (!self.user.password_confirmation) {
        error.password_confirmation = [];
        error.password_confirmation[0] = "Confirmation Password Field is required!";
      }

      if (self.user.position == "0") {
        error.position = [];
        error.position[0] = "Position Field is required!";
      }

      if (self.user.userRole == "0") {
        error.userRole = [];
        error.userRole[0] = "Roles Field is required!";
      }

      if (Object.keys(error).length) {
        self.$store.commit("setErrors", error);
        return true;
      }

      self.$store.commit("setErrors", {});
      return false;
    },
    store: function store() {
      var self = this;

      if (self.validationError()) {
        return;
      }

      this.is_loading = true;
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.post('/api/users?token=' + localStorage.getItem("api_token"), {
        name: self.user.name,
        employee_id: self.user.employee_id,
        email: self.user.email,
        joining_date: self.user.joining_date,
        position: self.user.position,
        role: self.user.userRole,
        password: self.user.password,
        password_confirmation: self.user.password_confirmation
      }).then(function (response) {
        self.is_loading = false;

        if (response) {
          self.$router.push({
            path: '/employees'
          });
        }
      });
    },
    countDownChanged: function countDownChanged(dismissCountDown) {
      this.dismissCountDown = dismissCountDown;
    },
    showAlert: function showAlert() {
      this.dismissCountDown = this.dismissSecs;
    },
    getDesignations: function getDesignations() {
      var self = this;
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('/api/designations?token=' + localStorage.getItem("api_token")).then(function (response) {
        self.items = response.data;
        self.$store.dispatch('topFunction');
      });
    }
  },
  mounted: function mounted() {
    var self = this;
    self.getDesignations();
    self.$store.commit("setErrors", {});
    axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('/api/roles?token=' + localStorage.getItem("api_token")).then(function (response) {
      self.roles = response.data;
    });
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/validation-errors.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/employees/validation-errors.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    error: {
      type: [Number, String, Array, Object],
      "default": 'Unknown'
    }
  },
  data: function data() {
    return {
      test: 'helo'
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/CreateEmployee.vue?vue&type=template&id=7309833e&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/employees/CreateEmployee.vue?vue&type=template&id=7309833e& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CRow",
    [
      _c(
        "CCol",
        { attrs: { col: "12", lg: "12" } },
        [
          _c(
            "CCard",
            [
              _c("CCardHeader", { staticClass: "task-heading mb-1" }, [
                _c(
                  "h3",
                  {
                    staticClass:
                      "d-flex justify-content-between align-items-center"
                  },
                  [
                    _vm._v("\n          Create Employee\n          "),
                    _c(
                      "div",
                      { staticClass: "btns-header" },
                      [
                        _c(
                          "CButton",
                          {
                            attrs: { color: "secondary" },
                            on: { click: _vm.goBack }
                          },
                          [_vm._v("Back")]
                        )
                      ],
                      1
                    )
                  ]
                )
              ]),
              _vm._v(" "),
              _c(
                "CCardBody",
                [
                  _c(
                    "CAlert",
                    {
                      attrs: {
                        show: _vm.dismissCountDown,
                        color: "primary",
                        fade: ""
                      },
                      on: {
                        "update:show": function($event) {
                          _vm.dismissCountDown = $event
                        }
                      }
                    },
                    [
                      _vm._v(
                        "\n          " +
                          _vm._s(_vm.message) +
                          "\n          \n        "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "CForm",
                    {
                      attrs: { method: "POST" },
                      on: {
                        submit: function($event) {
                          $event.preventDefault()
                          return _vm.store($event)
                        }
                      }
                    },
                    [
                      _c("CInput", {
                        attrs: {
                          label: "Employee Id *",
                          type: "number",
                          placeholder: "Employee Id"
                        },
                        model: {
                          value: _vm.user.employee_id,
                          callback: function($$v) {
                            _vm.$set(_vm.user, "employee_id", $$v)
                          },
                          expression: "user.employee_id"
                        }
                      }),
                      _vm._v(" "),
                      _vm.errors.employee_id
                        ? _c("validationError", {
                            attrs: { error: _vm.errors.employee_id[0] }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      _c("CInput", {
                        attrs: {
                          label: "Name *",
                          type: "text",
                          placeholder: "Name"
                        },
                        model: {
                          value: _vm.user.name,
                          callback: function($$v) {
                            _vm.$set(_vm.user, "name", $$v)
                          },
                          expression: "user.name"
                        }
                      }),
                      _vm._v(" "),
                      _vm.errors.name
                        ? _c("validationError", {
                            attrs: { error: _vm.errors.name[0] }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      _c("CInput", {
                        attrs: {
                          label: "Email *",
                          type: "email",
                          placeholder: "Email"
                        },
                        model: {
                          value: _vm.user.email,
                          callback: function($$v) {
                            _vm.$set(_vm.user, "email", $$v)
                          },
                          expression: "user.email"
                        }
                      }),
                      _vm._v(" "),
                      _vm.errors.email
                        ? _c("validationError", {
                            attrs: { error: _vm.errors.email[0] }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      _c("CInput", {
                        attrs: {
                          label: "Joining Date *",
                          placeholder: "Joining Date",
                          type: "date"
                        },
                        model: {
                          value: _vm.user.joining_date,
                          callback: function($$v) {
                            _vm.$set(_vm.user, "joining_date", $$v)
                          },
                          expression: "user.joining_date"
                        }
                      }),
                      _vm._v(" "),
                      _vm.errors.joining_date
                        ? _c("validation-error", {
                            attrs: { error: _vm.errors.joining_date[0] }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("label", [_vm._v(" Select Position *")]),
                        _vm._v(" "),
                        _c(
                          "select",
                          {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.user.position,
                                expression: "user.position"
                              }
                            ],
                            staticClass: "form-control",
                            on: {
                              change: function($event) {
                                var $$selectedVal = Array.prototype.filter
                                  .call($event.target.options, function(o) {
                                    return o.selected
                                  })
                                  .map(function(o) {
                                    var val = "_value" in o ? o._value : o.value
                                    return val
                                  })
                                _vm.$set(
                                  _vm.user,
                                  "position",
                                  $event.target.multiple
                                    ? $$selectedVal
                                    : $$selectedVal[0]
                                )
                              }
                            }
                          },
                          [
                            _c(
                              "option",
                              { attrs: { disabled: "", value: "0" } },
                              [_vm._v("Select a Position")]
                            ),
                            _vm._v(" "),
                            _vm._l(_vm.items, function(item) {
                              return _c(
                                "option",
                                { domProps: { value: item.id } },
                                [_vm._v(_vm._s(item.name))]
                              )
                            })
                          ],
                          2
                        )
                      ]),
                      _vm._v(" "),
                      _vm.errors.position
                        ? _c("validation-error", {
                            attrs: { error: _vm.errors.position[0] }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("label", [_vm._v(" Select role *")]),
                        _vm._v(" "),
                        _c(
                          "select",
                          {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.user.userRole,
                                expression: "user.userRole"
                              }
                            ],
                            staticClass: "form-control",
                            on: {
                              change: function($event) {
                                var $$selectedVal = Array.prototype.filter
                                  .call($event.target.options, function(o) {
                                    return o.selected
                                  })
                                  .map(function(o) {
                                    var val = "_value" in o ? o._value : o.value
                                    return val
                                  })
                                _vm.$set(
                                  _vm.user,
                                  "userRole",
                                  $event.target.multiple
                                    ? $$selectedVal
                                    : $$selectedVal[0]
                                )
                              }
                            }
                          },
                          [
                            _c(
                              "option",
                              { attrs: { disabled: "", value: "0" } },
                              [_vm._v("Select a Role")]
                            ),
                            _vm._v(" "),
                            _vm._l(_vm.roles, function(role) {
                              return _c(
                                "option",
                                { domProps: { value: role.name } },
                                [_vm._v(_vm._s(role.name))]
                              )
                            })
                          ],
                          2
                        )
                      ]),
                      _vm._v(" "),
                      _vm.errors.userRole
                        ? _c("validationError", {
                            attrs: { error: _vm.errors.userRole[0] }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      _c("CInput", {
                        attrs: {
                          label: "Password *",
                          type: "password",
                          placeholder: "Password"
                        },
                        model: {
                          value: _vm.user.password,
                          callback: function($$v) {
                            _vm.$set(_vm.user, "password", $$v)
                          },
                          expression: "user.password"
                        }
                      }),
                      _vm._v(" "),
                      _vm.errors.password
                        ? _c("validationError", {
                            attrs: { error: _vm.errors.password[0] }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      _c("CInput", {
                        attrs: {
                          label: "Reapeat Password *",
                          type: "password",
                          placeholder: "Reapeat Password"
                        },
                        model: {
                          value: _vm.user.password_confirmation,
                          callback: function($$v) {
                            _vm.$set(_vm.user, "password_confirmation", $$v)
                          },
                          expression: "user.password_confirmation"
                        }
                      }),
                      _vm._v(" "),
                      _vm.errors.password_confirmation
                        ? _c("validationError", {
                            attrs: {
                              error: _vm.errors.password_confirmation[0]
                            }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      _c(
                        "CButton",
                        { attrs: { color: "primary", type: "submit" } },
                        [_vm._v("Create")]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _vm.is_loading
        ? _c("div", { staticClass: "loader-image" }, [
            _c("img", { attrs: { src: _vm.loader } })
          ])
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/validation-errors.vue?vue&type=template&id=2961bf1b&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/employees/validation-errors.vue?vue&type=template&id=2961bf1b& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "form-group", attrs: { role: "group" } }, [
    _vm.error
      ? _c("span", { staticClass: "error" }, [_vm._v(_vm._s(_vm.error))])
      : _vm._e()
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);